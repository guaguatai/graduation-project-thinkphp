/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : exam43

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 24/12/2021 02:22:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '手机号',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '邮箱',
  `full_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '姓名',
  `head_portrait` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '头像',
  `psw` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `psw_salt` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '密码盐',
  `login_num` int(11) NOT NULL DEFAULT 0 COMMENT '登陆次数',
  `sort` int(11) NOT NULL DEFAULT 1000000 COMMENT '排序值',
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `login_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '排他性登陆标识',
  `last_login_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '最后登录IP',
  `last_login_time` int(11) DEFAULT 11 COMMENT '最后登录时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user`(`user`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 199 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 'admin', '10086', '10086@163.com', '超级管理员', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 1000000, '1', '0', '0', 11, '超级管理员', 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (3, 'lijiancheng001', '18077640001', '848446158@qq.com', '李建成001号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (4, 'lijiancheng002', '18077640002', '848446159@qq.com', '李建成002号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (5, 'lijiancheng003', '18077640003', '848446160@qq.com', '李建成003号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (6, 'lijiancheng004', '18077640004', '848446161@qq.com', '李建成004号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (7, 'lijiancheng005', '18077640005', '848446162@qq.com', '李建成005号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (8, 'lijiancheng006', '18077640006', '848446163@qq.com', '李建成006号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (9, 'lijiancheng007', '18077640007', '848446164@qq.com', '李建成007号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (10, 'lijiancheng008', '18077640008', '848446165@qq.com', '李建成008号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (11, 'lijiancheng009', '18077640009', '848446166@qq.com', '李建成009号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (12, 'lijiancheng010', '18077640010', '848446167@qq.com', '李建成010号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (13, 'lijiancheng011', '18077640011', '848446168@qq.com', '李建成011号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (14, 'lijiancheng012', '18077640012', '848446169@qq.com', '李建成012号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (15, 'lijiancheng013', '18077640013', '848446170@qq.com', '李建成013号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (16, 'lijiancheng014', '18077640014', '848446171@qq.com', '李建成014号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (17, 'lijiancheng015', '18077640015', '848446172@qq.com', '李建成015号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (18, 'lijiancheng016', '18077640016', '848446173@qq.com', '李建成016号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (19, 'lijiancheng017', '18077640017', '848446174@qq.com', '李建成017号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (20, 'lijiancheng018', '18077640018', '848446175@qq.com', '李建成018号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (21, 'lijiancheng019', '18077640019', '848446176@qq.com', '李建成019号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (22, 'lijiancheng020', '18077640020', '848446177@qq.com', '李建成020号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (23, 'lijiancheng021', '18077640021', '848446178@qq.com', '李建成021号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (24, 'lijiancheng022', '18077640022', '848446179@qq.com', '李建成022号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (25, 'lijiancheng023', '18077640023', '848446180@qq.com', '李建成023号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (26, 'lijiancheng024', '18077640024', '848446181@qq.com', '李建成024号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (27, 'lijiancheng025', '18077640025', '848446182@qq.com', '李建成025号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (28, 'lijiancheng026', '18077640026', '848446183@qq.com', '李建成026号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (29, 'lijiancheng027', '18077640027', '848446184@qq.com', '李建成027号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (30, 'lijiancheng028', '18077640028', '848446185@qq.com', '李建成028号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (31, 'lijiancheng029', '18077640029', '848446186@qq.com', '李建成029号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (32, 'lijiancheng030', '18077640030', '848446187@qq.com', '李建成030号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (33, 'lijiancheng031', '18077640031', '848446188@qq.com', '李建成031号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (34, 'lijiancheng032', '18077640032', '848446189@qq.com', '李建成032号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (35, 'lijiancheng033', '18077640033', '848446190@qq.com', '李建成033号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (36, 'lijiancheng034', '18077640034', '848446191@qq.com', '李建成034号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (37, 'lijiancheng035', '18077640035', '848446192@qq.com', '李建成035号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (38, 'lijiancheng036', '18077640036', '848446193@qq.com', '李建成036号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (39, 'lijiancheng037', '18077640037', '848446194@qq.com', '李建成037号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (40, 'lijiancheng038', '18077640038', '848446195@qq.com', '李建成038号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (41, 'lijiancheng039', '18077640039', '848446196@qq.com', '李建成039号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (42, 'lijiancheng040', '18077640040', '848446197@qq.com', '李建成040号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (43, 'lijiancheng041', '18077640041', '848446198@qq.com', '李建成041号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (44, 'lijiancheng042', '18077640042', '848446199@qq.com', '李建成042号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (45, 'lijiancheng043', '18077640043', '848446200@qq.com', '李建成043号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (46, 'lijiancheng044', '18077640044', '848446201@qq.com', '李建成044号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (47, 'lijiancheng045', '18077640045', '848446202@qq.com', '李建成045号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (48, 'lijiancheng046', '18077640046', '848446203@qq.com', '李建成046号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (49, 'lijiancheng047', '18077640047', '848446204@qq.com', '李建成047号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (50, 'lijiancheng048', '18077640048', '848446205@qq.com', '李建成048号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (51, 'lijiancheng049', '18077640049', '848446206@qq.com', '李建成049号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (52, 'lijiancheng050', '18077640050', '848446207@qq.com', '李建成050号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (53, 'lijiancheng051', '18077640051', '848446208@qq.com', '李建成051号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (54, 'lijiancheng052', '18077640052', '848446209@qq.com', '李建成052号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (55, 'lijiancheng053', '18077640053', '848446210@qq.com', '李建成053号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (56, 'lijiancheng054', '18077640054', '848446211@qq.com', '李建成054号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (57, 'lijiancheng055', '18077640055', '848446212@qq.com', '李建成055号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (58, 'lijiancheng056', '18077640056', '848446213@qq.com', '李建成056号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (59, 'lijiancheng057', '18077640057', '848446214@qq.com', '李建成057号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (60, 'lijiancheng058', '18077640058', '848446215@qq.com', '李建成058号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (61, 'lijiancheng059', '18077640059', '848446216@qq.com', '李建成059号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (62, 'lijiancheng060', '18077640060', '848446217@qq.com', '李建成060号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (63, 'lijiancheng061', '18077640061', '848446218@qq.com', '李建成061号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (64, 'lijiancheng062', '18077640062', '848446219@qq.com', '李建成062号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (65, 'lijiancheng063', '18077640063', '848446220@qq.com', '李建成063号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (66, 'lijiancheng064', '18077640064', '848446221@qq.com', '李建成064号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (67, 'lijiancheng065', '18077640065', '848446222@qq.com', '李建成065号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (68, 'lijiancheng066', '18077640066', '848446223@qq.com', '李建成066号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (69, 'lijiancheng067', '18077640067', '848446224@qq.com', '李建成067号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (70, 'lijiancheng068', '18077640068', '848446225@qq.com', '李建成068号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (71, 'lijiancheng069', '18077640069', '848446226@qq.com', '李建成069号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (72, 'lijiancheng070', '18077640070', '848446227@qq.com', '李建成070号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (73, 'lijiancheng071', '18077640071', '848446228@qq.com', '李建成071号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (74, 'lijiancheng072', '18077640072', '848446229@qq.com', '李建成072号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (75, 'lijiancheng073', '18077640073', '848446230@qq.com', '李建成073号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (76, 'lijiancheng074', '18077640074', '848446231@qq.com', '李建成074号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (77, 'lijiancheng075', '18077640075', '848446232@qq.com', '李建成075号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (78, 'lijiancheng076', '18077640076', '848446233@qq.com', '李建成076号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (79, 'lijiancheng077', '18077640077', '848446234@qq.com', '李建成077号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (80, 'lijiancheng078', '18077640078', '848446235@qq.com', '李建成078号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (81, 'lijiancheng079', '18077640079', '848446236@qq.com', '李建成079号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (82, 'lijiancheng080', '18077640080', '848446237@qq.com', '李建成080号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (83, 'lijiancheng081', '18077640081', '848446238@qq.com', '李建成081号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (84, 'lijiancheng082', '18077640082', '848446239@qq.com', '李建成082号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (85, 'lijiancheng083', '18077640083', '848446240@qq.com', '李建成083号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (86, 'lijiancheng084', '18077640084', '848446241@qq.com', '李建成084号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (87, 'lijiancheng085', '18077640085', '848446242@qq.com', '李建成085号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (88, 'lijiancheng086', '18077640086', '848446243@qq.com', '李建成086号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (89, 'lijiancheng087', '18077640087', '848446244@qq.com', '李建成087号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (90, 'lijiancheng088', '18077640088', '848446245@qq.com', '李建成088号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (91, 'lijiancheng089', '18077640089', '848446246@qq.com', '李建成089号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (92, 'lijiancheng090', '18077640090', '848446247@qq.com', '李建成090号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (93, 'lijiancheng091', '18077640091', '848446248@qq.com', '李建成091号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (94, 'lijiancheng092', '18077640092', '848446249@qq.com', '李建成092号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (95, 'lijiancheng093', '18077640093', '848446250@qq.com', '李建成093号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (96, 'lijiancheng094', '18077640094', '848446251@qq.com', '李建成094号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (97, 'lijiancheng095', '18077640095', '848446252@qq.com', '李建成095号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (98, 'lijiancheng096', '18077640096', '848446253@qq.com', '李建成096号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (99, 'lijiancheng097', '18077640097', '848446254@qq.com', '李建成097号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (100, 'lijiancheng098', '18077640098', '848446255@qq.com', '李建成098号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (101, 'lijiancheng099', '18077640099', '848446256@qq.com', '李建成099号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (102, 'lijiancheng100', '18077640100', '848446257@qq.com', '李建成100号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (103, 'lijiancheng101', '18077640101', '848446258@qq.com', '李建成101号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (104, 'lijiancheng102', '18077640102', '848446259@qq.com', '李建成102号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (105, 'lijiancheng103', '18077640103', '848446260@qq.com', '李建成103号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (106, 'lijiancheng104', '18077640104', '848446261@qq.com', '李建成104号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (107, 'lijiancheng105', '18077640105', '848446262@qq.com', '李建成105号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (108, 'lijiancheng106', '18077640106', '848446263@qq.com', '李建成106号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (109, 'lijiancheng107', '18077640107', '848446264@qq.com', '李建成107号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (110, 'lijiancheng108', '18077640108', '848446265@qq.com', '李建成108号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (111, 'lijiancheng109', '18077640109', '848446266@qq.com', '李建成109号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (112, 'lijiancheng110', '18077640110', '848446267@qq.com', '李建成110号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (113, 'lijiancheng111', '18077640111', '848446268@qq.com', '李建成111号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (114, 'lijiancheng112', '18077640112', '848446269@qq.com', '李建成112号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (115, 'lijiancheng113', '18077640113', '848446270@qq.com', '李建成113号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (116, 'lijiancheng114', '18077640114', '848446271@qq.com', '李建成114号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (117, 'lijiancheng115', '18077640115', '848446272@qq.com', '李建成115号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (118, 'lijiancheng116', '18077640116', '848446273@qq.com', '李建成116号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (119, 'lijiancheng117', '18077640117', '848446274@qq.com', '李建成117号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (120, 'lijiancheng118', '18077640118', '848446275@qq.com', '李建成118号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (121, 'lijiancheng119', '18077640119', '848446276@qq.com', '李建成119号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (122, 'lijiancheng120', '18077640120', '848446277@qq.com', '李建成120号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (123, 'lijiancheng121', '18077640121', '848446278@qq.com', '李建成121号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (124, 'lijiancheng122', '18077640122', '848446279@qq.com', '李建成122号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (125, 'lijiancheng123', '18077640123', '848446280@qq.com', '李建成123号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (126, 'lijiancheng124', '18077640124', '848446281@qq.com', '李建成124号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (127, 'lijiancheng125', '18077640125', '848446282@qq.com', '李建成125号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (128, 'lijiancheng126', '18077640126', '848446283@qq.com', '李建成126号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (129, 'lijiancheng127', '18077640127', '848446284@qq.com', '李建成127号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (130, 'lijiancheng128', '18077640128', '848446285@qq.com', '李建成128号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (131, 'lijiancheng129', '18077640129', '848446286@qq.com', '李建成129号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (132, 'lijiancheng130', '18077640130', '848446287@qq.com', '李建成130号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (133, 'lijiancheng131', '18077640131', '848446288@qq.com', '李建成131号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (134, 'lijiancheng132', '18077640132', '848446289@qq.com', '李建成132号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (135, 'lijiancheng133', '18077640133', '848446290@qq.com', '李建成133号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (136, 'lijiancheng134', '18077640134', '848446291@qq.com', '李建成134号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (137, 'lijiancheng135', '18077640135', '848446292@qq.com', '李建成135号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (138, 'lijiancheng136', '18077640136', '848446293@qq.com', '李建成136号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (139, 'lijiancheng137', '18077640137', '848446294@qq.com', '李建成137号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (140, 'lijiancheng138', '18077640138', '848446295@qq.com', '李建成138号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (141, 'lijiancheng139', '18077640139', '848446296@qq.com', '李建成139号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (142, 'lijiancheng140', '18077640140', '848446297@qq.com', '李建成140号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (143, 'lijiancheng141', '18077640141', '848446298@qq.com', '李建成141号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (144, 'lijiancheng142', '18077640142', '848446299@qq.com', '李建成142号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (145, 'lijiancheng143', '18077640143', '848446300@qq.com', '李建成143号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (146, 'lijiancheng144', '18077640144', '848446301@qq.com', '李建成144号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (147, 'lijiancheng145', '18077640145', '848446302@qq.com', '李建成145号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (148, 'lijiancheng146', '18077640146', '848446303@qq.com', '李建成146号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (149, 'lijiancheng147', '18077640147', '848446304@qq.com', '李建成147号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (150, 'lijiancheng148', '18077640148', '848446305@qq.com', '李建成148号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (151, 'lijiancheng149', '18077640149', '848446306@qq.com', '李建成149号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (152, 'lijiancheng150', '18077640150', '848446307@qq.com', '李建成150号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (153, 'lijiancheng151', '18077640151', '848446308@qq.com', '李建成151号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (154, 'lijiancheng152', '18077640152', '848446309@qq.com', '李建成152号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (155, 'lijiancheng153', '18077640153', '848446310@qq.com', '李建成153号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (156, 'lijiancheng154', '18077640154', '848446311@qq.com', '李建成154号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (157, 'lijiancheng155', '18077640155', '848446312@qq.com', '李建成155号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (158, 'lijiancheng156', '18077640156', '848446313@qq.com', '李建成156号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (159, 'lijiancheng157', '18077640157', '848446314@qq.com', '李建成157号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (160, 'lijiancheng158', '18077640158', '848446315@qq.com', '李建成158号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (161, 'lijiancheng159', '18077640159', '848446316@qq.com', '李建成159号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (162, 'lijiancheng160', '18077640160', '848446317@qq.com', '李建成160号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (163, 'lijiancheng161', '18077640161', '848446318@qq.com', '李建成161号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (164, 'lijiancheng162', '18077640162', '848446319@qq.com', '李建成162号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (165, 'lijiancheng163', '18077640163', '848446320@qq.com', '李建成163号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (166, 'lijiancheng164', '18077640164', '848446321@qq.com', '李建成164号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (167, 'lijiancheng165', '18077640165', '848446322@qq.com', '李建成165号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (168, 'lijiancheng166', '18077640166', '848446323@qq.com', '李建成166号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (169, 'lijiancheng167', '18077640167', '848446324@qq.com', '李建成167号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (170, 'lijiancheng168', '18077640168', '848446325@qq.com', '李建成168号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (171, 'lijiancheng169', '18077640169', '848446326@qq.com', '李建成169号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (172, 'lijiancheng170', '18077640170', '848446327@qq.com', '李建成170号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (173, 'lijiancheng171', '18077640171', '848446328@qq.com', '李建成171号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (174, 'lijiancheng172', '18077640172', '848446329@qq.com', '李建成172号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (175, 'lijiancheng173', '18077640173', '848446330@qq.com', '李建成173号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (176, 'lijiancheng174', '18077640174', '848446331@qq.com', '李建成174号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (177, 'lijiancheng175', '18077640175', '848446332@qq.com', '李建成175号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (178, 'lijiancheng176', '18077640176', '848446333@qq.com', '李建成176号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (179, 'lijiancheng177', '18077640177', '848446334@qq.com', '李建成177号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (180, 'lijiancheng178', '18077640178', '848446335@qq.com', '李建成178号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (181, 'lijiancheng179', '18077640179', '848446336@qq.com', '李建成179号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (182, 'lijiancheng180', '18077640180', '848446337@qq.com', '李建成180号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (183, 'lijiancheng181', '18077640181', '848446338@qq.com', '李建成181号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (184, 'lijiancheng182', '18077640182', '848446339@qq.com', '李建成182号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (185, 'lijiancheng183', '18077640183', '848446340@qq.com', '李建成183号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (186, 'lijiancheng184', '18077640184', '848446341@qq.com', '李建成184号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (187, 'lijiancheng185', '18077640185', '848446342@qq.com', '李建成185号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (188, 'lijiancheng186', '18077640186', '848446343@qq.com', '李建成186号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (189, 'lijiancheng187', '18077640187', '848446344@qq.com', '李建成187号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (190, 'lijiancheng188', '18077640188', '848446345@qq.com', '李建成188号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (191, 'lijiancheng189', '18077640189', '848446346@qq.com', '李建成189号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (192, 'lijiancheng190', '18077640190', '848446347@qq.com', '李建成190号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (193, 'lijiancheng191', '18077640191', '848446348@qq.com', '李建成191号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (194, 'lijiancheng192', '18077640192', '848446349@qq.com', '李建成192号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (195, 'lijiancheng193', '18077640193', '848446350@qq.com', '李建成193号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (196, 'lijiancheng194', '18077640194', '848446351@qq.com', '李建成194号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (197, 'lijiancheng195', '18077640195', '848446352@qq.com', '李建成195号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);
INSERT INTO `admin` VALUES (198, 'lijiancheng196', '18077640196', '848446353@qq.com', '李建成196号', NULL, '$2y$10$LVEOpgloCzgZoBfMd4rAcuo5GoKYHdaD/iEFtXlKiPpiFSSe9QL7a', 'IXrZKNYv', 0, 0, '1', '0', '0', 0, NULL, 1631013234, 1634042339, 0);

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `gname` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '分组名称',
  `menu_ids` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '菜单集合',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `sort` int(11) DEFAULT 0 COMMENT '排序值',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注信息',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '分组表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES (1, '超级管理员', '*', 'fa fa-users', 1, 1, '超级管理员，拥有所有权限', 1631013234, 1636009898, 0);
INSERT INTO `group` VALUES (2, '管理员', '13,4,7', 'fa fa-user', 1, 2, '普通管理员', 1636009679, 1638271823, 0);
INSERT INTO `group` VALUES (3, '普通用户', '12,7', 'fa fa-user-circle', 1, 3, '普通用户', 1636009780, 1638271837, 0);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(11) NOT NULL DEFAULT 0 COMMENT '父ID',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `icon` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `href` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '链接',
  `type` enum('node','menu','button','function') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'menu' COMMENT '菜单类型:node=节点,menu=菜单,button=按钮,function=功能',
  `target` enum('_self','_blank','_parent','_top') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` int(11) DEFAULT 0 COMMENT '菜单排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '备注信息',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `title`(`title`) USING BTREE,
  INDEX `href`(`href`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统菜单表' ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, 0, '系统管理', 'fa fa-cog', '', 'node', '_self', 1, 1, '', 1635247847, 1635923987, 0);
INSERT INTO `menu` VALUES (2, 1, '用户管理', 'fa fa-user', '/admin/admin/index', 'menu', '_self', 1, 1, '', 1635247847, 1635925363, 0);
INSERT INTO `menu` VALUES (3, 1, '菜单管理', 'fa fa-navicon', '/admin/menu/index', 'menu', '_self', 2, 1, '', 1635247847, 1635924323, 0);
INSERT INTO `menu` VALUES (4, 1, '分组管理', 'fa fa-users', '/admin/group/index', 'menu', '_self', 3, 1, '', 1635247847, 1635925324, 0);
INSERT INTO `menu` VALUES (5, 1, '用户分组管理', 'fa fa-cubes', '/admin/usergroup/index', 'menu', '_self', 4, 1, '', 1635247847, 1635926169, 0);
INSERT INTO `menu` VALUES (6, 0, '内容管理', 'fa fa-cog', '', 'node', '_self', 2, 1, NULL, 1635926169, 1635926169, 0);
INSERT INTO `menu` VALUES (7, 6, '栏目管理', 'fa fa-navicon', '', 'menu', '_self', 1, 1, NULL, 1635926169, 1635926169, 0);
INSERT INTO `menu` VALUES (8, 6, '新闻管理', 'fa fa-navicon', '', 'menu', '_self', 2, 1, NULL, 1635926169, 1635926169, 0);
INSERT INTO `menu` VALUES (9, 6, '测试', 'fa fa-adn', '', 'menu', '_self', 3, 1, '测试', 1637066277, 1637066277, 0);
INSERT INTO `menu` VALUES (10, 2, '添加', 'fa fa-chrome', '/admin/admin/create', 'button', '_self', 1, 1, '', 1638000314, 1638000314, 0);
INSERT INTO `menu` VALUES (11, 2, '编辑', 'fa fa-adn', '/admin/admin/edit', 'button', '_self', 2, 1, '', 1638000524, 1638000618, 0);
INSERT INTO `menu` VALUES (12, 2, '删除', 'fa fa-adjust', '/admin/admin/delete', 'button', '_self', 3, 1, '', 1638000571, 1638000609, 0);
INSERT INTO `menu` VALUES (13, 3, '获取菜单树', '', '/admin/menu/getTreeList', 'function', '_self', 1, 1, '', 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (14, 0, '生产车间信息管理', 'fa fa-adjust', '', 'node', '_self', 3, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (15, 14, '车间信息管理', 'fa fa-american-sign-language-interpreting', '/admin/spworkshop/index', 'menu', '_self', 1, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (17, 14, '设备信息管理', 'fa fa-plug', '/admin/spdevice/index', 'menu', '_self', 2, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (18, 14, '员工信息管理', 'fa fa-address-card-o', '/admin/sppersonnel/index', 'menu', '_self', 3, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (19, 14, '公告信息管理', 'fa fa-bullhorn', '/admin/spnotice/index', 'menu', '_self', 4, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (20, 14, '物料信息管理', 'fa fa-thumb-tack', '/admin/spmaterial/index', 'menu', '_self', 5, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (21, 14, '产品信息管理', 'fa fa-keyboard-o', '/admin/spproduct/index', 'menu', '_self', 6, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (22, 14, '订单信息管理', 'fa fa-ambulance', '/admin/sporder/index', 'menu', '_self', 7, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (23, 14, '故障设备管理', 'fa fa-exclamation-circle', '/admin/spfault/index', 'menu', '_self', 8, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (24, 14, '员工签到管理', 'fa fa-paper-plane-o', '/admin/spemployeesign/index', 'menu', '_self', 9, 1, NULL, 1638003062, 1638003062, 0);
INSERT INTO `menu` VALUES (25, 14, '产品质检管理', 'fa fa-registered', '/admin/spqualityinspection/index', 'menu', '_self', 10, 1, NULL, 1, 1, 0);

-- ----------------------------
-- Table structure for sp_device
-- ----------------------------
DROP TABLE IF EXISTS `sp_device`;
CREATE TABLE `sp_device`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '设备编号',
  `dev_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '设备名称',
  `dev_date` date NOT NULL COMMENT '生产日期',
  `dev_manufacturer` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '生产商',
  `dev_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备照片',
  `dev_price` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备价格',
  `dev_power` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备功率',
  `dev_type` enum('办公设备','生产设备','辅助设备') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备类型',
  `dev_status` enum('正在使用','已关机') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '正在使用' COMMENT '设备状态',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '添加时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_device
-- ----------------------------
INSERT INTO `sp_device` VALUES (1, '质检灯', '2021-11-04', '青岛青天环境工程有限公司', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '99', '22V', '办公设备', '已关机', '无', 1638277596, 1638278658, 0);
INSERT INTO `sp_device` VALUES (2, '风刀烘干机', '2021-11-05', '石狮市兴达机械厂', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '388', '30V', '办公设备', '已关机', '无', 1638277596, 1638278658, 0);
INSERT INTO `sp_device` VALUES (3, '智能感应灌溉机', '2021-11-06', '石狮市兴达机械厂', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '32000', '200V', '生产设备', '正在使用', '无', 1638277596, 1639035712, 0);
INSERT INTO `sp_device` VALUES (4, '微孔筒式过滤器', '2021-11-07', '石狮市兴达机械厂', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '59888', '200V', '办公设备', '已关机', '无', 1638277596, 1640280044, 0);
INSERT INTO `sp_device` VALUES (5, '清洗机', '2021-11-08', '青岛青天环境工程有限公司', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '3000', '200V', '办公设备', '已关机', '无', 1638277596, 1638278658, 0);
INSERT INTO `sp_device` VALUES (6, '打印机', '2021-11-09', '青岛青天环境工程有限公司', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '888', '22V', '办公设备', '已关机', '无', 1638277596, 1638278658, 0);
INSERT INTO `sp_device` VALUES (7, '多项运动混合机', '2021-11-10', '石狮市兴达机械厂', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '59999', '100V', '办公设备', '已关机', '无', 1638277596, 1638278658, 0);
INSERT INTO `sp_device` VALUES (8, '高度旋转式混合机', '2021-11-11', '石狮市兴达机械厂', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '100000', '100V', '办公设备', '已关机', '无', 1638277596, 1638278658, 0);
INSERT INTO `sp_device` VALUES (9, '空调', '2021-11-12', '德州东润空调设备有限公司', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '8888', '32V', '办公设备', '已关机', '无', 1638277596, 1638278658, 0);

-- ----------------------------
-- Table structure for sp_employeesign
-- ----------------------------
DROP TABLE IF EXISTS `sp_employeesign`;
CREATE TABLE `sp_employeesign`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '签到编号',
  `emp_name` int(11) NOT NULL COMMENT '员工姓名',
  `emp_date` int(11) NOT NULL COMMENT '签到时间',
  `emp_status` enum('正常签到','未签到','请假','缺勤') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '正常签到' COMMENT '签到状态',
  `emp_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '签到照片',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 402 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_employeesign
-- ----------------------------
INSERT INTO `sp_employeesign` VALUES (1, 13, 1639055177, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1, 1640282434, 0);
INSERT INTO `sp_employeesign` VALUES (2, 18, 1639054248, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055208, 0);
INSERT INTO `sp_employeesign` VALUES (3, 2, 1610200085, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (4, 3, 1610200085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (5, 4, 1610200085, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (6, 5, 1610200085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (7, 6, 1610200085, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (8, 7, 1610200085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (9, 8, 1610200085, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (10, 9, 1631261905, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (11, 10, 1631261905, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (12, 11, 1631261905, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (13, 12, 1631261905, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (14, 13, 1631261905, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (15, 14, 1631261905, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (16, 15, 1631261905, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (17, 16, 1631261905, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (18, 17, 1631261905, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (19, 18, 1631261905, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (20, 2, 1631261905, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (21, 3, 1631261905, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (22, 4, 1631261905, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (23, 5, 1631261905, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (24, 6, 1631261905, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (25, 7, 1631261905, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (26, 8, 1631261905, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (27, 9, 1612878485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (28, 10, 1612878485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (29, 11, 1612878485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (30, 12, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (31, 13, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (32, 14, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (33, 15, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (34, 16, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (35, 17, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (36, 18, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (37, 19, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (38, 20, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (39, 21, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (40, 22, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (41, 23, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (42, 24, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (43, 25, 1612878485, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (44, 26, 1615297685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (45, 27, 1615297685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (46, 28, 1615297685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (47, 29, 1615297685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (48, 30, 1615297685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (49, 31, 1615297685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (50, 32, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (51, 33, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (52, 34, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (53, 35, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (54, 36, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (55, 37, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (56, 38, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (57, 39, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (58, 40, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (59, 41, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (60, 2, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (61, 3, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (62, 4, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (63, 5, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (64, 6, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (65, 7, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (66, 8, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (67, 9, 1615297685, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (68, 10, 1617976085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (69, 11, 1617976085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (70, 12, 1617976085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (71, 13, 1617976085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (72, 14, 1617976085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (73, 15, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (74, 16, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (75, 17, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (76, 18, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (77, 19, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (78, 20, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (79, 21, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (80, 22, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (81, 23, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (82, 24, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (83, 25, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (84, 26, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (85, 27, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (86, 28, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (87, 29, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (88, 30, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (89, 31, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (90, 32, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (91, 33, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (92, 34, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (93, 35, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (94, 36, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (95, 37, 1617976085, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (96, 38, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (97, 39, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (98, 40, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (99, 41, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (100, 2, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (101, 3, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (102, 4, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (103, 5, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (104, 6, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (105, 7, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (106, 8, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (107, 9, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (108, 10, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (109, 11, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (110, 12, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (111, 13, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (112, 14, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (113, 15, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (114, 16, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (115, 17, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (116, 18, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (117, 19, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (118, 20, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (119, 21, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (120, 22, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (121, 23, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (122, 24, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (123, 25, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (124, 26, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (125, 27, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (126, 28, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (127, 29, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (128, 30, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (129, 31, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (130, 32, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (131, 33, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (132, 34, 1617976085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (133, 35, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (134, 36, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (135, 37, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (136, 38, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (137, 39, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (138, 40, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (139, 41, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (140, 2, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (141, 3, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (142, 4, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (143, 5, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (144, 6, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (145, 7, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (146, 8, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (147, 9, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (148, 10, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (149, 11, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (150, 12, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (151, 13, 1620568085, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (152, 14, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (153, 15, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (154, 16, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (155, 17, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (156, 18, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (157, 19, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (158, 20, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (159, 21, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (160, 22, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (161, 23, 1620568085, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (162, 24, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (163, 25, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (164, 26, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (165, 27, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (166, 28, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (167, 29, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (168, 30, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (169, 31, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (170, 32, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (171, 33, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (172, 34, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (173, 35, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (174, 36, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (175, 37, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (176, 38, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (177, 39, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (178, 40, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (179, 41, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (180, 2, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (181, 3, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (182, 4, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (183, 5, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (184, 6, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (185, 7, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (186, 8, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (187, 9, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (188, 10, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (189, 11, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (190, 12, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (191, 13, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (192, 14, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (193, 15, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (194, 16, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (195, 17, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (196, 18, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (197, 19, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (198, 20, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (199, 21, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (200, 22, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (201, 23, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (202, 24, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (203, 25, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (204, 26, 1623246485, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (205, 27, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (206, 28, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (207, 29, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (208, 30, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (209, 31, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (210, 32, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (211, 33, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (212, 34, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (213, 35, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (214, 36, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (215, 37, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (216, 38, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (217, 39, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (218, 40, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (219, 41, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (220, 2, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (221, 3, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (222, 4, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (223, 5, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (224, 6, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (225, 7, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (226, 8, 1623246485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (227, 9, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (228, 10, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (229, 11, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (230, 12, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (231, 13, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (232, 14, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (233, 15, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (234, 16, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (235, 17, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (236, 18, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (237, 19, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (238, 20, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (239, 21, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (240, 22, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (241, 23, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (242, 24, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (243, 25, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (244, 26, 1625838485, '缺勤', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (245, 38, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (246, 39, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (247, 40, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (248, 41, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (249, 2, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (250, 3, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (251, 4, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (252, 5, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (253, 6, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (254, 7, 1628516885, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (255, 8, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (256, 9, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (257, 10, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (258, 11, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (259, 12, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (260, 13, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (261, 14, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (262, 15, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (263, 16, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (264, 17, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (265, 18, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (266, 19, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (267, 20, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (268, 21, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (269, 22, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (270, 23, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (271, 24, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (272, 25, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (273, 26, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (274, 27, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (275, 28, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (276, 29, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (277, 30, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (278, 31, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (279, 32, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (280, 33, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (281, 34, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (282, 35, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (283, 36, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (284, 37, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (285, 38, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (286, 39, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (287, 40, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (288, 41, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (289, 2, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (290, 3, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (291, 4, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (292, 5, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (293, 6, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (294, 7, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (295, 8, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (296, 9, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (297, 10, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (298, 11, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (299, 12, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (300, 13, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (301, 14, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (302, 15, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (303, 16, 1633787285, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (304, 17, 1633787285, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (305, 18, 1633787285, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (306, 19, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (307, 20, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (308, 21, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (309, 22, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (310, 23, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (311, 24, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (312, 25, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (313, 26, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (314, 27, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (315, 28, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (316, 29, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (317, 30, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (318, 31, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (319, 32, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (320, 33, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (321, 34, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (322, 35, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (323, 36, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (324, 37, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (325, 38, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (326, 39, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (327, 40, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (328, 41, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (329, 2, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (330, 3, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (331, 4, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (332, 5, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (333, 6, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (334, 7, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (335, 8, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (336, 9, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (337, 10, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (338, 11, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (339, 12, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (340, 13, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (341, 14, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (342, 15, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (343, 16, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (344, 17, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (345, 18, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (346, 19, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (347, 20, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (348, 21, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (349, 22, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (350, 23, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (351, 24, 1636465685, '未签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (352, 9, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (353, 10, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (354, 11, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (355, 12, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (356, 13, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (357, 14, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (358, 15, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (359, 16, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (360, 17, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (361, 18, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (362, 19, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (363, 20, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (364, 21, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (365, 22, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (366, 23, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (367, 24, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (368, 25, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (369, 26, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (370, 27, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (371, 28, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (372, 29, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (373, 30, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (374, 31, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (375, 32, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (376, 33, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (377, 34, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (378, 35, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (379, 36, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (380, 37, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (381, 38, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (382, 39, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (383, 40, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (384, 41, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (385, 2, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (386, 3, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (387, 4, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (388, 5, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (389, 6, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (390, 7, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (391, 8, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (392, 9, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (393, 10, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (394, 11, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (395, 12, 1636465685, '正常签到', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (396, 13, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (397, 14, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (398, 15, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (399, 16, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);
INSERT INTO `sp_employeesign` VALUES (400, 17, 1636465685, '请假', 'storage/images/20211224\\6e3211124da82a17f2d12cfe4dba7f86.jpeg', '无', 1639054253, 1639055195, 0);

-- ----------------------------
-- Table structure for sp_fault
-- ----------------------------
DROP TABLE IF EXISTS `sp_fault`;
CREATE TABLE `sp_fault`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '故障编号',
  `fau_dev` int(11) NOT NULL COMMENT '设备名称',
  `fau_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '设备照片',
  `fau_date` date NOT NULL COMMENT '故障时间',
  `fau_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '故障原因',
  `per_jobnumber` int(11) NOT NULL COMMENT '故障核实人员',
  `fau_status` enum('故障中','已维修') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '故障中' COMMENT '故障状态',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 175 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_fault
-- ----------------------------
INSERT INTO `sp_fault` VALUES (1, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-07', '人为损坏', 14, '故障中', '无', 1639048995, 1639050380, 0);
INSERT INTO `sp_fault` VALUES (2, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-24', '人为损坏', 14, '故障中', '无', 1639048995, 1639050388, 0);
INSERT INTO `sp_fault` VALUES (3, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-26', '设备堵塞', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (4, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-01-07', '胶布用完了连续工作导致烧机', 14, '故障中', '无', 1639048995, 1639050380, 0);
INSERT INTO `sp_fault` VALUES (5, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-01-08', '设备老旧', 14, '已维修', '无', 1639048995, 1639050388, 0);
INSERT INTO `sp_fault` VALUES (6, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-01-09', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (7, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-01-10', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (8, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-01-11', '人为损坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (9, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-01-12', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (10, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-01-13', '设备堵塞', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (11, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-01-14', '摔坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (12, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-01-15', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (13, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-16', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (14, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-17', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (15, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-18', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (16, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-19', '电路不通', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (17, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-20', '工作量太大烧坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (18, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-21', '胶布用完了连续工作导致烧机', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (19, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-22', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (20, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-23', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (21, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-24', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (22, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-25', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (23, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-26', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (24, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-27', '设备堵塞', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (25, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-02-28', '摔坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (26, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-01', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (27, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-02', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (28, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-03', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (29, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-04', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (30, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-05', '电路不通', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (31, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-06', '工作量太大烧坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (32, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-07', '胶布用完了连续工作导致烧机', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (33, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-08', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (34, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-09', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (35, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-03-10', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (36, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-11', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (37, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-12', '人为损坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (38, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-13', '设备堵塞', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (39, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-14', '摔坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (40, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-15', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (41, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-16', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (42, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-17', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (43, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-18', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (44, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-19', '电路不通', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (45, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-20', '工作量太大烧坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (46, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-21', '胶布用完了连续工作导致烧机', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (47, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-22', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (48, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-23', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (49, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-24', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (50, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-04-25', '人为损坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (51, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-05-26', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (52, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-05-27', '设备堵塞', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (53, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-05-28', '摔坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (54, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-05-29', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (55, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-05-30', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (56, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-05-31', '开关坏了', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (57, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-01', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (58, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-02', '电路不通', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (59, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-03', '工作量太大烧坏了', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (60, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-04', '胶布用完了连续工作导致烧机', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (61, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-05', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (62, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-06', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (63, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-07', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (64, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-08', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (65, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-09', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (66, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-10', '设备堵塞', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (67, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-11', '摔坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (68, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-12', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (69, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-13', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (70, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-14', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (71, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-15', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (72, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-16', '电路不通', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (73, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-06-17', '工作量太大烧坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (74, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-18', '胶布用完了连续工作导致烧机', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (75, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-19', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (76, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-20', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (77, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-21', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (78, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-22', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (79, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-23', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (80, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-24', '设备堵塞', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (81, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-25', '摔坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (82, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-26', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (83, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-27', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (84, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-28', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (85, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-29', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (86, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-30', '电路不通', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (87, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-07-31', '工作量太大烧坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (88, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-01', '胶布用完了连续工作导致烧机', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (89, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-02', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (90, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-03', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (91, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-04', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (92, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-05', '人为损坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (93, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-06', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (94, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-07', '设备堵塞', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (95, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-08', '摔坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (96, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-09', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (97, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-10', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (98, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-11', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (99, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-12', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (100, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-08-13', '电路不通', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (101, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-14', '工作量太大烧坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (102, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-15', '胶布用完了连续工作导致烧机', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (103, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-16', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (104, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-17', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (105, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-18', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (106, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-19', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (107, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-20', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (108, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-21', '设备堵塞', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (109, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-22', '摔坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (110, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-23', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (111, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-24', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (112, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-25', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (113, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-26', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (114, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-27', '电路不通', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (115, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-28', '工作量太大烧坏了', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (116, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-29', '胶布用完了连续工作导致烧机', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (117, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-09-30', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (118, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-01', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (119, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-02', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (120, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-03', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (121, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-04', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (122, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-05', '设备堵塞', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (123, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-06', '摔坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (124, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-07', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (125, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-08', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (126, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-09', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (127, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-10', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (128, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-11', '电路不通', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (129, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-12', '工作量太大烧坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (130, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-13', '胶布用完了连续工作导致烧机', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (131, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-14', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (132, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-15', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (133, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-16', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (134, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-17', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (135, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-18', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (136, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-19', '设备堵塞', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (137, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-20', '摔坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (138, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-21', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (139, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-22', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (140, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-23', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (141, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-24', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (142, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-25', '电路不通', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (143, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-26', '工作量太大烧坏了', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (144, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-27', '胶布用完了连续工作导致烧机', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (145, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-28', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (146, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-29', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (147, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-30', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (148, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-10-31', '人为损坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (149, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-01', '人为损坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (150, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-02', '设备堵塞', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (151, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-03', '摔坏', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (152, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-04', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (153, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-05', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (154, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-06', '开关坏了', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (155, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-07', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (156, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-08', '电路不通', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (157, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-09', '工作量太大烧坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (158, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-10', '胶布用完了连续工作导致烧机', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (159, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-11', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (160, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-12', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (161, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-13', '设备老旧', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (162, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-14', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (163, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-15', '人为损坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (164, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-16', '设备堵塞', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (165, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-11-17', '摔坏', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (166, 3, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-18', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (167, 4, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-19', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (168, 5, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-20', '开关坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (169, 6, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-21', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (170, 7, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-22', '电路不通', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (171, 8, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-23', '工作量太大烧坏了', 14, '已维修', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (172, 9, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-24', '胶布用完了连续工作导致烧机', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (173, 10, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-25', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);
INSERT INTO `sp_fault` VALUES (174, 11, 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-26', '设备老旧', 14, '故障中', '无', 1639049449, 1639050070, 0);

-- ----------------------------
-- Table structure for sp_material
-- ----------------------------
DROP TABLE IF EXISTS `sp_material`;
CREATE TABLE `sp_material`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '物料编号',
  `mat_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '物料名称',
  `mat_quantity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '物料数量',
  `mat_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '物料照片',
  `mat_date` date NOT NULL COMMENT '采购日期',
  `mat_per` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '采购人',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_material
-- ----------------------------
INSERT INTO `sp_material` VALUES (1, '耳塞', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-09', '李建成', '无', 1638883577, 1640281113, 0);
INSERT INTO `sp_material` VALUES (2, '口罩', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-30', '李建成', '无', 1638879564, 1638883577, 0);
INSERT INTO `sp_material` VALUES (3, '螺丝', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-09', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (4, '螺丝刀', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-30', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (5, '电线', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2021-12-31', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (6, '手套', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-01', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (7, '电子板', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-02', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (8, '显示屏', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-03', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (9, '无尘服', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-04', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (10, '集成电阻', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-05', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (11, '五号电池', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-06', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (12, '弹簧', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-07', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (13, 'USB接口', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-08', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (14, '扳手', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-09', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (15, '高分子按钮', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-10', '李建成', '无', 1638883577, 1638879309, 0);
INSERT INTO `sp_material` VALUES (16, '胶水', '19568', 'storage/images/20211224\\be334248db9b50085eed0789bfa7d8e7.jpeg', '2022-01-11', '李建成', '无', 1638883577, 1638879309, 0);

-- ----------------------------
-- Table structure for sp_notice
-- ----------------------------
DROP TABLE IF EXISTS `sp_notice`;
CREATE TABLE `sp_notice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '公告编号',
  `not_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `not_con` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告内容',
  `not_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公告图片',
  `not_time` int(11) NOT NULL COMMENT '发布时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `delete_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_notice
-- ----------------------------
INSERT INTO `sp_notice` VALUES (1, '春节上班时间调整通知', '经公司研究决定，春节放假时间定于20xx年1月25日(大年二十五)至20xx年2月7日(正月初八)共计\n13天。20xx年2月7日(正月初八)上午10点整开门营业，现将各专柜需注意事项通知如下：\n　　1、20xx年1月24 日18：00商场放假停止营业，各商铺须将贵重物品带离商场;\n　　2、商铺工作人员须将专柜商品以外的物品(含饰品)用纸箱打包编号，自行处理或存放于商铺内;\n　　3、商铺工作人员须在20xx年1月24日18：00前清理商铺内所有垃圾到指定地点;\n　　4、商铺工作人员须在20xx年1月24日18：00关闭专柜总电源(配电箱处)，拨下专柜所有用电插头\n并将专柜入口用封箱胶带或其他物品封好，放假后任何人员(包括本专柜人员)均不得入内;\n　　5、所有专柜人员在20xx年1月24日18：00后须无条件离开商场，到20xx年2月7日10：00前，均\n不得以任何理由进入商场。', 'storage/images/20211224\\da70218f2566c63eccf66f4a2e568872.jpeg', 1638878598, '无', 1638878598, 1640280889, 0);
INSERT INTO `sp_notice` VALUES (2, '开展职业安全健康教育', '开展职业安全健康教育工作,普及和提高全体员工的职业安全健康知识,提高自救、互救能力。', 'storage/images/20211224\\da70218f2566c63eccf66f4a2e568872.jpeg', 1638878598, '无', 1638878598, 1638878598, 0);
INSERT INTO `sp_notice` VALUES (3, '关于如何快速敲完十个表的数据', '第一步：000，第二步：111', 'storage/images/20211224\\da70218f2566c63eccf66f4a2e568872.jpeg', 1638878598, '无', 1638877877, 1640245313, 0);

-- ----------------------------
-- Table structure for sp_order
-- ----------------------------
DROP TABLE IF EXISTS `sp_order`;
CREATE TABLE `sp_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单编号',
  `pro_name` int(11) NOT NULL COMMENT '产品名称',
  `ord_buyers` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '顾客姓名',
  `ord_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '产品图片',
  `ord_date` int(11) NOT NULL COMMENT '成交时间',
  `ord_quantity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '数量',
  `ord_price` int(10) NOT NULL DEFAULT 0 COMMENT '价格',
  `ord_status` enum('已发货','未发货','已收货','退款') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '订单状态',
  `ord_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '收货地址',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '订单备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 579 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_order
-- ----------------------------
INSERT INTO `sp_order` VALUES (1, 3, '尤淑芬', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街001号', '无2', 1639040053, 1640281732, 0);
INSERT INTO `sp_order` VALUES (2, 1, '安晓', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '已发货', '广西壮族自治区百色市那坡县城厢镇镇玉街001号', '无', 1639040053, 1639040053, 0);
INSERT INTO `sp_order` VALUES (3, 1, '闾丘纨', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1639955165, '1', 69, '已收货', '广西壮族自治区百色市那坡县城厢镇镇玉街001号', '无', 1639041179, 1639041645, 0);
INSERT INTO `sp_order` VALUES (4, 1, '钱敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1639042234, '1', 69, '已发货', '广西壮族自治区百色市那坡县城厢镇镇玉街001号', '无', 1639042246, 1640244032, 0);
INSERT INTO `sp_order` VALUES (5, 1, '孙丽红', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (6, 1, '何梅香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (7, 1, '隆迎春', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '已发货', '广西壮族自治区百色市那坡县城厢镇镇玉街003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (8, 1, '吕子鑫', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '已收货', '广西壮族自治区百色市那坡县城厢镇镇玉街004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (9, 1, '姜松', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '已收货', '广西壮族自治区百色市那坡县城厢镇镇玉街005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (10, 1, '乐佳玲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (11, 1, '何之念', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '退款', '广西壮族自治区百色市那坡县城厢镇镇玉街007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (12, 1, '王寻文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (13, 1, '沈婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (14, 3, '周媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已发货', '广西壮族自治区百色市那坡县城厢镇镇玉街010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (15, 4, '赵小蝶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '广西壮族自治区百色市那坡县城厢镇镇玉街011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (16, 5, '陈叶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '广西壮族自治区百色市那坡县城厢镇镇玉街012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (17, 6, '魏招弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (18, 3, '赵岚', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '退款', '广西壮族自治区百色市那坡县城厢镇镇玉街014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (19, 2, '郑阳涌', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (20, 2, '卫春萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街016号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (21, 2, '许雅瑄', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已发货', '广西壮族自治区百色市那坡县城厢镇镇玉街017号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (22, 2, '尤倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已收货', '广西壮族自治区百色市那坡县城厢镇镇玉街018号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (23, 2, '张山柳', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已收货', '广西壮族自治区百色市那坡县城厢镇镇玉街019号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (24, 2, '戚滢', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街020号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (25, 2, '施乐之', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '退款', '广西壮族自治区百色市那坡县城厢镇镇玉街021号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (26, 2, '金平夏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街022号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (27, 2, '李沛文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '广西壮族自治区百色市那坡县城厢镇镇玉街023号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (28, 2, '尤淑芬', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已发货', '广西壮族自治区百色市那坡县城厢镇镇玉街024号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (29, 2, '安晓', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已收货', '广东省广州市白云区青春大道001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (30, 2, '闾丘纨', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已收货', '广东省广州市白云区青春大道002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (31, 2, '钱敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '广东省广州市白云区青春大道003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (32, 3, '孙秀丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '退款', '广东省广州市白云区青春大道004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (33, 3, '赵碧香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '广东省广州市白云区青春大道005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (34, 3, '杨静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '广东省广州市白云区青春大道006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (35, 3, '孔丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已发货', '广东省广州市白云区青春大道007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (36, 3, '韦开萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '广东省广州市白云区青春大道008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (37, 3, '施慧', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '广东省广州市白云区青春大道009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (38, 3, '张德群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '广东省广州市白云区青春大道010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (39, 3, '施倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '退款', '广东省广州市白云区青春大道011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (40, 3, '韩心媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '广东省广州市白云区青春大道012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (41, 3, '冯恩珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '广东省广州市白云区青春大道013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (42, 4, '窦幻波', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已发货', '广东省广州市白云区青春大道014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (43, 5, '秦寒', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '广东省广州市白云区青春大道015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (44, 6, '何聪', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '四川省成都市大山区大学路001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (45, 7, '钱媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '四川省成都市大山区大学路002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (46, 8, '魏南莲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '四川省成都市大山区大学路003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (47, 9, '吕婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '四川省成都市大山区大学路004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (48, 10, '郎静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '四川省成都市大山区大学路005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (49, 11, '沈露', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已发货', '四川省成都市大山区大学路006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (50, 3, '陶姣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '四川省成都市大山区大学路007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (51, 4, '何乐', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '四川省成都市大山区大学路008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (52, 5, '尤兴', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '四川省成都市大山区大学路009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (53, 6, '周娣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '四川省成都市大山区大学路010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (54, 7, '朱翠翠', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '四川省成都市大山区大学路011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (55, 8, '沈树平', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '四川省成都市大山区大学路012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (56, 9, '秦元琼', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '四川省成都市大山区大学路013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (57, 10, '戚紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '四川省成都市大山区大学路014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (58, 11, '许莎', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已收货', '四川省成都市大山区大学路015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (59, 3, '魏青', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '四川省成都市大山区大学路016号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (60, 4, '吕倩倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '退款', '四川省成都市大山区大学路017号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (61, 5, '吕子彤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '四川省成都市大山区大学路018号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (62, 6, '周杰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '四川省成都市大山区大学路019号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (63, 7, '尤小兰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已发货', '四川省成都市大山区大学路020号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (64, 8, '严凡', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '湖南省长沙市橘子洲头001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (65, 9, '孔梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '湖南省长沙市橘子洲头002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (66, 10, '秦香秀', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '湖南省长沙市橘子洲头003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (67, 11, '陈丹', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '退款', '湖南省长沙市橘子洲头004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (68, 3, '韩政群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '湖南省长沙市橘子洲头005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (69, 4, '何火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '湖南省长沙市橘子洲头006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (70, 5, '尤琦', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已发货', '湖南省长沙市橘子洲头007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (71, 6, '何婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '湖南省长沙市橘子洲头008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (72, 7, '金梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '湖南省长沙市橘子洲头009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (73, 8, '卫紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '湖南省长沙市橘子洲头010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (74, 9, '施玉亭', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '湖南省长沙市橘子洲头011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (75, 10, '华凯敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '湖南省长沙市橘子洲头012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (76, 11, '吴融', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '湖南省长沙市橘子洲头013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (77, 3, '李韵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已发货', '湖南省长沙市橘子洲头014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (78, 4, '金倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '湖南省长沙市橘子洲头015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (79, 4, '卫勤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '湖南省长沙市橘子洲头016号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (80, 5, '卫显', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '湖南省长沙市橘子洲头017号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (81, 5, '漆雕万敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '退款', '湖南省长沙市橘子洲头018号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (82, 5, '喻贞', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '湖南省长沙市橘子洲头019号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (83, 5, '朱发弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '湖南省长沙市橘子洲头020号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (84, 5, '韩凌珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已发货', '湖南省长沙市橘子洲头021号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (85, 5, '施火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '湖南省长沙市橘子洲头022号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (86, 5, '张晓磊', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '湖南省长沙市橘子洲头023号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (87, 5, '孙丽红', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '湖南省长沙市橘子洲头024号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (88, 5, '何梅香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '退款', '湖南省长沙市橘子洲头025号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (89, 6, '隆迎春', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '湖南省长沙市橘子洲头026号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (90, 6, '吕子鑫', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '湖南省长沙市橘子洲头027号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (91, 6, '姜松', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '湖南省长沙市橘子洲头028号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (92, 6, '乐佳玲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '湖南省长沙市橘子洲头029号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (93, 6, '何之念', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '湖南省长沙市橘子洲头030号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (94, 6, '王寻文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '湖南省长沙市橘子洲头031号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (95, 6, '沈婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '湖南省长沙市橘子洲头032号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (96, 6, '周媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (97, 6, '赵小蝶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (98, 6, '陈叶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '北京市王府井大街003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (99, 6, '魏招弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (100, 6, '赵岚', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (101, 6, '郑阳涌', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (102, 6, '卫春萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '北京市王府井大街007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (103, 6, '许雅瑄', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (104, 6, '尤倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (105, 6, '张山柳', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '北京市王府井大街010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (106, 6, '戚滢', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (107, 6, '施乐之', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (108, 6, '金平夏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (109, 6, '李沛文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '北京市王府井大街014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (110, 6, '尤淑芬', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (111, 6, '安晓', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街016号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (112, 6, '闾丘纨', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '北京市王府井大街017号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (113, 6, '钱敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街018号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (114, 6, '孙秀丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街019号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (115, 6, '赵碧香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街020号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (116, 6, '杨静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '北京市王府井大街021号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (117, 6, '孔丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街022号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (118, 6, '韦开萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街023号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (119, 6, '施慧', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '北京市王府井大街024号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (120, 6, '张德群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街025号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (121, 6, '施倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街026号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (122, 6, '韩心媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街027号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (123, 6, '冯恩珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '北京市王府井大街028号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (124, 6, '窦幻波', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街029号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (125, 6, '秦寒', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街030号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (126, 6, '何聪', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '北京市王府井大街031号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (127, 6, '钱媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街032号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (128, 6, '魏南莲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街033号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (129, 6, '吕婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街034号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (130, 6, '郎静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '北京市王府井大街035号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (131, 6, '沈露', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街036号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (132, 2, '陶姣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '北京市王府井大街037号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (133, 2, '何乐', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已发货', '北京市王府井大街038号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (134, 2, '尤兴', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已收货', '北京市王府井大街039号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (135, 2, '周娣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已收货', '北京市王府井大街040号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (136, 2, '朱翠翠', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '北京市王府井大街041号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (137, 2, '沈树平', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '退款', '北京市王府井大街042号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (138, 2, '秦元琼', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '北京市王府井大街043号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (139, 2, '戚紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '北京市王府井大街044号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (140, 2, '许莎', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已发货', '北京市王府井大街045号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (141, 2, '魏青', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已收货', '北京市王府井大街046号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (142, 2, '吕倩倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '已收货', '北京市王府井大街047号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (143, 2, '吕子彤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '北京市王府井大街048号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (144, 2, '周杰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '退款', '北京市王府井大街049号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (145, 2, '尤小兰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 23, '未发货', '北京市王府井大街050号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (146, 9, '严凡', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '北京市王府井大街051号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (147, 10, '孔梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已发货', '北京市王府井大街052号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (148, 11, '秦香秀', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已收货', '北京市王府井大街053号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (149, 3, '陈丹', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '北京市王府井大街054号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (150, 4, '韩政群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '北京市王府井大街055号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (151, 5, '何火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '退款', '北京市王府井大街056号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (152, 6, '尤琦', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街057号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (153, 7, '何婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '北京市王府井大街058号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (154, 8, '金梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '北京市王府井大街059号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (155, 8, '卫紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街060号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (156, 8, '施玉亭', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街061号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (157, 8, '华凯敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街062号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (158, 8, '吴融', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '北京市王府井大街063号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (159, 8, '李韵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街064号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (160, 8, '金倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街065号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (161, 8, '卫勤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '北京市王府井大街066号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (162, 8, '卫显', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街067号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (163, 8, '漆雕万敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街068号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (164, 8, '喻贞', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街069号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (165, 8, '朱发弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '北京市王府井大街070号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (166, 11, '韩凌珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '北京市王府井大街071号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (167, 3, '施火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '北京市王府井大街072号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (168, 4, '张晓磊', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已发货', '北京市王府井大街073号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (169, 5, '孙丽红', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '北京市王府井大街074号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (170, 6, '何梅香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街075号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (171, 7, '隆迎春', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '北京市王府井大街076号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (172, 8, '吕子鑫', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '北京市王府井大街077号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (173, 9, '姜松', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '北京市王府井大街078号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (174, 10, '乐佳玲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '北京市王府井大街079号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (175, 11, '何之念', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已发货', '北京市王府井大街080号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (176, 3, '王寻文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '北京市王府井大街081号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (177, 4, '沈婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '北京市王府井大街082号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (178, 5, '周媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '北京市王府井大街083号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (179, 6, '赵小蝶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '北京市王府井大街084号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (180, 7, '陈叶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '北京市王府井大街085号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (181, 8, '魏招弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街086号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (182, 9, '赵岚', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '北京市王府井大街087号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (183, 10, '郑阳涌', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '北京市王府井大街088号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (184, 11, '卫春萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已收货', '北京市王府井大街089号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (185, 3, '许雅瑄', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '北京市王府井大街090号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (186, 4, '尤倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '退款', '北京市王府井大街091号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (187, 5, '张山柳', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '北京市王府井大街092号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (188, 6, '戚滢', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街093号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (189, 7, '施乐之', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已发货', '北京市王府井大街094号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (190, 8, '金平夏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街095号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (191, 9, '李沛文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '北京市王府井大街096号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (192, 10, '尤淑芬', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '北京市王府井大街097号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (193, 11, '安晓', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '退款', '北京市王府井大街098号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (194, 3, '闾丘纨', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '北京市王府井大街099号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (195, 4, '钱敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '北京市王府井大街100号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (196, 5, '孙秀丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已发货', '北京市王府井大街101号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (197, 6, '赵碧香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '北京市王府井大街102号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (198, 7, '杨静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '北京市王府井大街103号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (199, 8, '孔丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '北京市王府井大街104号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (200, 9, '韦开萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '北京市王府井大街105号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (201, 10, '施慧', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '北京市王府井大街106号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (202, 9, '张德群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '北京市王府井大街107号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (203, 9, '施倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '北京市王府井大街108号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (204, 9, '韩心媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省大连市永宁区大学城001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (205, 9, '冯恩珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省大连市永宁区大学城002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (206, 9, '窦幻波', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (207, 9, '秦寒', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省大连市永宁区大学城004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (208, 9, '何聪', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (209, 9, '钱媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (210, 9, '魏南莲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '辽宁省大连市永宁区大学城007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (211, 9, '吕婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省大连市永宁区大学城008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (212, 9, '郎静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省大连市永宁区大学城009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (213, 9, '沈露', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (214, 9, '陶姣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省大连市永宁区大学城011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (215, 9, '何乐', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (216, 9, '尤兴', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (217, 9, '周娣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '辽宁省大连市永宁区大学城014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (218, 9, '朱翠翠', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省大连市永宁区大学城015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (219, 9, '沈树平', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省大连市永宁区大学城016号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (220, 9, '秦元琼', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城017号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (221, 9, '戚紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省大连市永宁区大学城018号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (222, 9, '许莎', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城019号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (223, 9, '魏青', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城020号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (224, 9, '吕倩倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '辽宁省大连市永宁区大学城021号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (225, 9, '吕子彤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省大连市永宁区大学城022号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (226, 9, '周杰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省大连市永宁区大学城023号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (227, 9, '尤小兰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城024号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (228, 9, '严凡', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省大连市永宁区大学城025号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (229, 9, '孔梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省大连市永宁区大学城026号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (230, 9, '秦香秀', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学001宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (231, 9, '陈丹', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '贵州省贵阳市贵安区贵州师范大学002宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (232, 9, '韩政群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学003宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (233, 9, '何火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学004宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (234, 9, '尤琦', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学005宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (235, 9, '何婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '贵州省贵阳市贵安区贵州师范大学006宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (236, 9, '金梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学007宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (237, 9, '卫紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学008宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (238, 9, '施玉亭', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '贵州省贵阳市贵安区贵州师范大学009宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (239, 9, '华凯敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学010宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (240, 9, '吴融', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学011宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (241, 9, '李韵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学012宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (242, 9, '金倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '贵州省贵阳市贵安区贵州师范大学013宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (243, 9, '卫勤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学014宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (244, 9, '卫显', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学015宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (245, 9, '漆雕万敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '贵州省贵阳市贵安区贵州师范大学016宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (246, 9, '喻贞', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学017宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (247, 9, '朱发弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学018宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (248, 9, '韩凌珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学019宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (249, 9, '施火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '贵州省贵阳市贵安区贵州师范大学020宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (250, 9, '张晓磊', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学021宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (251, 9, '孙丽红', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学022宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (252, 9, '何梅香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '贵州省贵阳市贵安区贵州师范大学023宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (253, 9, '隆迎春', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学024宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (254, 9, '吕子鑫', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学025宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (255, 9, '姜松', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学026宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (256, 9, '乐佳玲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '贵州省贵阳市贵安区贵州师范大学027宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (257, 9, '何之念', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学028宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (258, 9, '王寻文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学029宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (259, 9, '沈婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '贵州省贵阳市贵安区贵州师范大学030宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (260, 9, '周媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学031宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (261, 9, '赵小蝶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学032宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (262, 9, '陈叶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学033宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (263, 9, '魏招弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '贵州省贵阳市贵安区贵州师范大学034宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (264, 9, '赵岚', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学035宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (265, 9, '郑阳涌', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '贵州省贵阳市贵安区贵州师范大学036宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (266, 9, '卫春萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '贵州省贵阳市贵安区贵州师范大学037宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (267, 9, '许雅瑄', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学038宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (268, 9, '尤倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '贵州省贵阳市贵安区贵州师范大学039宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (269, 9, '张山柳', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '云南省文山自治县富宁街001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (270, 9, '戚滢', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '云南省文山自治县富宁街002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (271, 9, '施乐之', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '云南省文山自治县富宁街003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (272, 9, '金平夏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '云南省文山自治县富宁街004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (273, 9, '李沛文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '云南省文山自治县富宁街005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (274, 9, '尤淑芬', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '云南省文山自治县富宁街006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (275, 9, '安晓', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '云南省文山自治县富宁街007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (276, 9, '闾丘纨', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '云南省文山自治县富宁街008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (277, 9, '钱敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '云南省文山自治县富宁街009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (278, 9, '孙秀丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '云南省文山自治县富宁街010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (279, 9, '赵碧香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '云南省文山自治县富宁街011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (280, 9, '杨静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '云南省文山自治县富宁街012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (281, 9, '孔丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '云南省文山自治县富宁街013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (282, 9, '韦开萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '云南省文山自治县富宁街014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (283, 9, '施慧', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '云南省文山自治县富宁街015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (284, 9, '张德群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '云南省文山自治县富宁街016号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (285, 9, '施倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '云南省文山自治县富宁街017号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (286, 9, '韩心媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '云南省文山自治县富宁街018号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (287, 9, '冯恩珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '云南省文山自治县富宁街019号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (288, 1, '窦幻波', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '已收货', '云南省文山自治县富宁街020号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (289, 1, '秦寒', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '已收货', '云南省文山自治县富宁街021号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (290, 1, '何聪', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '云南省文山自治县富宁街022号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (291, 1, '钱媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '退款', '云南省文山自治县富宁街023号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (292, 1, '魏南莲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '云南省文山自治县富宁街024号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (293, 1, '吕婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '云南省文山自治县富宁街025号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (294, 1, '郎静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '已发货', '云南省文山自治县富宁街026号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (295, 1, '沈露', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '已收货', '云南省文山自治县富宁街027号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (296, 1, '陶姣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '已收货', '云南省文山自治县富宁街028号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (297, 1, '何乐', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '云南省文山自治县富宁街029号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (298, 1, '尤兴', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '退款', '云南省文山自治县富宁街030号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (299, 1, '周娣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 69, '未发货', '云南省文山自治县富宁街031号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (300, 9, '朱翠翠', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (301, 11, '沈树平', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (302, 3, '秦元琼', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (303, 4, '戚紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (304, 5, '许莎', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (305, 6, '魏青', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (306, 7, '吕倩倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (307, 8, '吕子彤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (308, 9, '周杰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (309, 10, '尤小兰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (310, 11, '严凡', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已收货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (311, 3, '孔梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (312, 4, '秦香秀', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '退款', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (313, 5, '陈丹', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (314, 6, '韩政群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (315, 7, '何火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (316, 8, '尤琦', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (317, 9, '何婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (318, 10, '金梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (319, 11, '卫紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '退款', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (320, 3, '施玉亭', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (321, 4, '华凯敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (322, 5, '吴融', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (323, 6, '李韵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (324, 7, '金倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (325, 8, '卫勤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (326, 9, '卫显', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '海南省海南大学', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (327, 10, '漆雕万敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '福建省厦门市厦门大学001宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (328, 11, '喻贞', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '福建省厦门市厦门大学002宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (329, 3, '朱发弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已发货', '福建省厦门市厦门大学003宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (330, 4, '韩凌珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '福建省厦门市厦门大学004宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (331, 5, '施火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '福建省厦门市厦门大学005宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (332, 6, '张晓磊', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '福建省厦门市厦门大学006宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (333, 7, '孙丽红', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '退款', '福建省厦门市厦门大学007宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (334, 8, '何梅香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '福建省厦门市厦门大学008宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (335, 9, '隆迎春', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '福建省厦门市厦门大学009宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (336, 10, '吕子鑫', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已发货', '福建省厦门市厦门大学010宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (337, 11, '姜松', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已收货', '福建省厦门市厦门大学011宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (338, 3, '乐佳玲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '福建省厦门市厦门大学012宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (339, 4, '何之念', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '福建省厦门市厦门大学013宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (340, 5, '王寻文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '退款', '福建省厦门市厦门大学014宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (341, 6, '沈婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '福建省厦门市厦门大学015宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (342, 7, '周媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '福建省厦门市厦门大学016宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (343, 8, '赵小蝶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '福建省厦门市厦门大学017宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (344, 9, '陈叶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '福建省厦门市厦门大学018宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (345, 10, '魏招弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '福建省厦门市厦门大学019宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (346, 11, '赵岚', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '福建省厦门市厦门大学020宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (347, 3, '郑阳涌', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '退款', '福建省厦门市厦门大学021宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (348, 4, '卫春萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '福建省厦门市厦门大学022宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (349, 5, '许雅瑄', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '福建省厦门市厦门大学023宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (350, 6, '尤倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '福建省厦门市厦门大学024宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (351, 7, '张山柳', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '福建省厦门市厦门大学025宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (352, 8, '戚滢', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '福建省厦门市厦门大学026宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (353, 9, '施乐之', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '福建省厦门市厦门大学027宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (354, 10, '金平夏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '福建省厦门市厦门大学028宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (355, 11, '李沛文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '福建省厦门市厦门大学029宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (356, 3, '尤淑芬', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '福建省厦门市厦门大学030宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (357, 4, '安晓', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已发货', '福建省厦门市厦门大学031宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (358, 5, '闾丘纨', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '福建省厦门市厦门大学032宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (359, 6, '钱敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '福建省厦门市厦门大学033宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (360, 7, '孙秀丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '福建省厦门市厦门大学034宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (361, 8, '赵碧香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '福建省厦门市厦门大学035宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (362, 9, '杨静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '福建省厦门市厦门大学036宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (363, 10, '孔丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '福建省厦门市厦门大学037宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (364, 11, '韦开萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已发货', '福建省厦门市厦门大学038宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (365, 10, '施慧', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '福建省厦门市厦门大学039宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (366, 10, '张德群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '福建省厦门市厦门大学040宿舍', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (367, 10, '施倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (368, 10, '韩心媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '浙江省金华市华南皮革厂002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (369, 10, '冯恩珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (370, 10, '窦幻波', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (371, 10, '秦寒', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已发货', '浙江省金华市华南皮革厂005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (372, 10, '何聪', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '浙江省金华市华南皮革厂006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (373, 10, '钱媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '浙江省金华市华南皮革厂007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (374, 10, '魏南莲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (375, 10, '吕婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '浙江省金华市华南皮革厂009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (376, 10, '郎静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (377, 10, '沈露', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (378, 10, '陶姣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已发货', '浙江省金华市华南皮革厂012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (379, 10, '何乐', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '浙江省金华市华南皮革厂013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (380, 10, '尤兴', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '浙江省金华市华南皮革厂014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (381, 10, '周娣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (382, 10, '朱翠翠', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '浙江省金华市华南皮革厂016号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (383, 10, '沈树平', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂017号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (384, 10, '秦元琼', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂018号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (385, 10, '戚紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已发货', '浙江省金华市华南皮革厂019号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (386, 6, '许莎', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '浙江省金华市华南皮革厂020号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (387, 7, '魏青', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '浙江省金华市华南皮革厂021号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (388, 8, '吕倩倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '浙江省金华市华南皮革厂022号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (389, 9, '吕子彤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '浙江省金华市华南皮革厂023号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (390, 10, '周杰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '浙江省金华市华南皮革厂024号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (391, 11, '尤小兰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '浙江省金华市华南皮革厂025号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (392, 3, '严凡', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已发货', '浙江省金华市华南皮革厂026号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (393, 4, '孔梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '浙江省金华市华南皮革厂027号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (394, 5, '秦香秀', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '浙江省金华市华南皮革厂028号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (395, 6, '陈丹', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '浙江省金华市华南皮革厂029号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (396, 7, '韩政群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '退款', '浙江省金华市华南皮革厂030号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (397, 8, '何火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '浙江省金华市华南皮革厂031号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (398, 9, '尤琦', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '浙江省金华市华南皮革厂032号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (399, 10, '何婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已发货', '浙江省金华市华南皮革厂033号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (400, 11, '金梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已收货', '浙江省金华市华南皮革厂034号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (401, 3, '卫紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '浙江省金华市华南皮革厂035号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (402, 4, '施玉亭', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '浙江省金华市华南皮革厂036号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (403, 5, '华凯敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '退款', '浙江省金华市华南皮革厂037号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (404, 6, '吴融', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '浙江省金华市华南皮革厂038号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (405, 7, '李韵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '浙江省金华市华南皮革厂039号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (406, 8, '金倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '浙江省金华市华南皮革厂040号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (407, 9, '卫勤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '浙江省金华市华南皮革厂041号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (408, 10, '卫显', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '浙江省金华市华南皮革厂042号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (409, 11, '漆雕万敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '浙江省金华市华南皮革厂043号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (410, 3, '喻贞', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '退款', '浙江省金华市华南皮革厂044号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (411, 4, '朱发弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '浙江省金华市华南皮革厂045号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (412, 5, '韩凌珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '浙江省金华市华南皮革厂046号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (413, 6, '施火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '浙江省金华市华南皮革厂047号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (414, 7, '张晓磊', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '浙江省金华市华南皮革厂048号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (415, 8, '孙丽红', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '陕西省秦始皇陵那个地方001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (416, 9, '何梅香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '陕西省秦始皇陵那个地方002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (417, 10, '隆迎春', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '陕西省秦始皇陵那个地方003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (418, 11, '吕子鑫', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '陕西省秦始皇陵那个地方004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (419, 3, '姜松', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '陕西省秦始皇陵那个地方005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (420, 4, '乐佳玲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已发货', '陕西省秦始皇陵那个地方006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (421, 5, '何之念', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '陕西省秦始皇陵那个地方007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (422, 6, '王寻文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '陕西省秦始皇陵那个地方008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (423, 7, '沈婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '陕西省秦始皇陵那个地方009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (424, 8, '周媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '陕西省秦始皇陵那个地方010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (425, 9, '赵小蝶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '陕西省秦始皇陵那个地方011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (426, 10, '陈叶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '陕西省秦始皇陵那个地方012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (427, 11, '魏招弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已发货', '陕西省秦始皇陵那个地方013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (428, 3, '赵岚', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '陕西省秦始皇陵那个地方014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (429, 4, '郑阳涌', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '陕西省秦始皇陵那个地方015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (430, 5, '卫春萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '陕西省秦始皇陵那个地方016号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (431, 6, '许雅瑄', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '陕西省秦始皇陵那个地方017号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (432, 7, '尤倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '陕西省秦始皇陵那个地方018号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (433, 8, '张山柳', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '陕西省秦始皇陵那个地方019号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (434, 9, '戚滢', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '陕西省秦始皇陵那个地方020号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (435, 10, '施乐之', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '陕西省秦始皇陵那个地方021号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (436, 11, '金平夏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已收货', '陕西省秦始皇陵那个地方022号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (437, 3, '李沛文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '陕西省秦始皇陵那个地方023号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (438, 4, '尤淑芬', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '退款', '陕西省秦始皇陵那个地方024号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (439, 5, '安晓', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '陕西省秦始皇陵那个地方025号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (440, 6, '闾丘纨', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '陕西省秦始皇陵那个地方026号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (441, 7, '钱敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已发货', '陕西省秦始皇陵那个地方027号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (442, 8, '孙秀丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '陕西省秦始皇陵那个地方028号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (443, 9, '赵碧香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '陕西省秦始皇陵那个地方029号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (444, 10, '杨静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '陕西省秦始皇陵那个地方030号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (445, 11, '孔丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '退款', '陕西省秦始皇陵那个地方031号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (446, 3, '韦开萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '辽宁省辽宁市那个好冷好冷的地方001号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (447, 4, '施慧', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '辽宁省辽宁市那个好冷好冷的地方002号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (448, 5, '张德群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已发货', '辽宁省辽宁市那个好冷好冷的地方003号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (449, 6, '施倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '辽宁省辽宁市那个好冷好冷的地方004号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (450, 9, '韩心媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省辽宁市那个好冷好冷的地方005号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (451, 9, '冯恩珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方006号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (452, 9, '窦幻波', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省辽宁市那个好冷好冷的地方007号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (453, 9, '秦寒', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方008号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (454, 9, '何聪', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方009号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (455, 9, '钱媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '辽宁省辽宁市那个好冷好冷的地方010号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (456, 9, '魏南莲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省辽宁市那个好冷好冷的地方011号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (457, 9, '吕婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省辽宁市那个好冷好冷的地方012号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (458, 9, '郎静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方013号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (459, 9, '沈露', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省辽宁市那个好冷好冷的地方014号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (460, 9, '陶姣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方015号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (461, 9, '何乐', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方016号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (462, 9, '尤兴', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '辽宁省辽宁市那个好冷好冷的地方017号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (463, 9, '周娣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省辽宁市那个好冷好冷的地方018号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (464, 9, '朱翠翠', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省辽宁市那个好冷好冷的地方019号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (465, 9, '沈树平', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方020号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (466, 9, '秦元琼', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省辽宁市那个好冷好冷的地方021号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (467, 9, '戚紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方022号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (468, 9, '许莎', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方023号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (469, 9, '魏青', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '辽宁省辽宁市那个好冷好冷的地方024号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (470, 9, '吕倩倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省辽宁市那个好冷好冷的地方025号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (471, 9, '吕子彤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省辽宁市那个好冷好冷的地方026号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (472, 9, '周杰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方027号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (473, 9, '尤小兰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省辽宁市那个好冷好冷的地方028号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (474, 9, '严凡', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方029号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (475, 9, '孔梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方030号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (476, 6, '秦香秀', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '辽宁省辽宁市那个好冷好冷的地方031号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (477, 7, '陈丹', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '辽宁省辽宁市那个好冷好冷的地方032号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (478, 8, '韩政群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '辽宁省辽宁市那个好冷好冷的地方033号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (479, 9, '何火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方034号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (480, 10, '尤琦', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '辽宁省辽宁市那个好冷好冷的地方035号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (481, 11, '何婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '辽宁省辽宁市那个好冷好冷的地方036号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (482, 3, '金梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '辽宁省辽宁市那个好冷好冷的地方037号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (483, 4, '卫紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已发货', '辽宁省辽宁市那个好冷好冷的地方038号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (484, 10, '施玉亭', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '辽宁省辽宁市那个好冷好冷的地方039号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (485, 10, '华凯敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '辽宁省辽宁市那个好冷好冷的地方040号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (486, 10, '吴融', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方041号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (487, 10, '李韵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '辽宁省辽宁市那个好冷好冷的地方042号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (488, 10, '金倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方043号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (489, 10, '卫勤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方044号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (490, 10, '卫显', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已发货', '辽宁省辽宁市那个好冷好冷的地方045号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (491, 10, '漆雕万敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '辽宁省辽宁市那个好冷好冷的地方046号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (492, 10, '喻贞', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '辽宁省辽宁市那个好冷好冷的地方047号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (493, 10, '朱发弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方048号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (494, 10, '韩凌珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '辽宁省辽宁市那个好冷好冷的地方049号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (495, 10, '施火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方050号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (496, 10, '张晓磊', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方051号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (497, 10, '孙丽红', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已发货', '辽宁省辽宁市那个好冷好冷的地方052号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (498, 10, '何梅香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '辽宁省辽宁市那个好冷好冷的地方053号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (499, 10, '隆迎春', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '辽宁省辽宁市那个好冷好冷的地方054号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (500, 10, '吕子鑫', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方055号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (501, 10, '姜松', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '辽宁省辽宁市那个好冷好冷的地方056号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (502, 10, '乐佳玲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方057号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (503, 10, '何之念', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方058号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (504, 7, '王寻文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已发货', '辽宁省辽宁市那个好冷好冷的地方059号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (505, 8, '沈婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '辽宁省辽宁市那个好冷好冷的地方060号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (506, 9, '周媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省辽宁市那个好冷好冷的地方061号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (507, 10, '赵小蝶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方062号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (508, 11, '陈叶', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '退款', '辽宁省辽宁市那个好冷好冷的地方063号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (509, 3, '魏招弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '辽宁省辽宁市那个好冷好冷的地方064号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (510, 4, '赵岚', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '辽宁省辽宁市那个好冷好冷的地方065号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (511, 5, '郑阳涌', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已发货', '辽宁省辽宁市那个好冷好冷的地方066号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (512, 6, '卫春萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '辽宁省辽宁市那个好冷好冷的地方067号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (513, 7, '许雅瑄', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '辽宁省辽宁市那个好冷好冷的地方068号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (514, 8, '尤倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '辽宁省辽宁市那个好冷好冷的地方069号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (515, 9, '张山柳', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '辽宁省辽宁市那个好冷好冷的地方070号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (516, 10, '戚滢', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方071号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (517, 11, '施乐之', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '辽宁省辽宁市那个好冷好冷的地方072号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (518, 3, '金平夏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已发货', '辽宁省辽宁市那个好冷好冷的地方073号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (519, 4, '李沛文', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '辽宁省辽宁市那个好冷好冷的地方074号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (520, 5, '尤淑芬', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '辽宁省辽宁市那个好冷好冷的地方075号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (521, 6, '安晓', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '辽宁省辽宁市那个好冷好冷的地方076号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (522, 7, '闾丘纨', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '退款', '辽宁省辽宁市那个好冷好冷的地方077号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (523, 8, '钱敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '辽宁省辽宁市那个好冷好冷的地方078号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (524, 9, '孙秀丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方079号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (525, 10, '赵碧香', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已发货', '辽宁省辽宁市那个好冷好冷的地方080号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (526, 11, '杨静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已收货', '辽宁省辽宁市那个好冷好冷的地方081号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (527, 3, '孔丽', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '辽宁省辽宁市那个好冷好冷的地方082号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (528, 4, '韦开萍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '辽宁省辽宁市那个好冷好冷的地方083号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (529, 5, '施慧', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '退款', '辽宁省辽宁市那个好冷好冷的地方084号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (530, 6, '张德群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '辽宁省辽宁市那个好冷好冷的地方085号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (531, 7, '施倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '退款', '辽宁省辽宁市那个好冷好冷的地方086号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (532, 8, '韩心媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '辽宁省辽宁市那个好冷好冷的地方087号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (533, 9, '冯恩珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '辽宁省辽宁市那个好冷好冷的地方088号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (534, 10, '窦幻波', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '已收货', '辽宁省辽宁市那个好冷好冷的地方089号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (535, 11, '秦寒', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '辽宁省辽宁市那个好冷好冷的地方090号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (536, 3, '何聪', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '退款', '辽宁省辽宁市那个好冷好冷的地方091号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (537, 4, '钱媛', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '辽宁省辽宁市那个好冷好冷的地方092号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (538, 5, '魏南莲', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '辽宁省辽宁市那个好冷好冷的地方093号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (539, 6, '吕婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已发货', '辽宁省辽宁市那个好冷好冷的地方094号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (540, 7, '郎静', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '辽宁省辽宁市那个好冷好冷的地方095号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (541, 8, '沈露', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '辽宁省辽宁市那个好冷好冷的地方096号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (542, 9, '陶姣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方097号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (543, 10, '何乐', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '辽宁省辽宁市那个好冷好冷的地方098号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (544, 11, '尤兴', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '未发货', '辽宁省辽宁市那个好冷好冷的地方099号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (545, 3, '周娣', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '辽宁省辽宁市那个好冷好冷的地方100号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (546, 4, '朱翠翠', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已发货', '辽宁省辽宁市那个好冷好冷的地方101号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (547, 5, '沈树平', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已收货', '辽宁省辽宁市那个好冷好冷的地方102号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (548, 6, '秦元琼', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '辽宁省辽宁市那个好冷好冷的地方103号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (549, 7, '戚紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '辽宁省辽宁市那个好冷好冷的地方104号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (550, 8, '许莎', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '辽宁省辽宁市那个好冷好冷的地方105号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (551, 9, '魏青', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '未发货', '辽宁省辽宁市那个好冷好冷的地方106号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (552, 10, '吕倩倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '辽宁省辽宁市那个好冷好冷的地方107号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (553, 11, '吕子彤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已发货', '辽宁省辽宁市那个好冷好冷的地方108号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (554, 3, '周杰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '已收货', '辽宁省辽宁市那个好冷好冷的地方109号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (555, 4, '尤小兰', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '已收货', '辽宁省辽宁市那个好冷好冷的地方110号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (556, 5, '严凡', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '辽宁省辽宁市那个好冷好冷的地方111号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (557, 6, '孔梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '退款', '辽宁省辽宁市那个好冷好冷的地方112号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (558, 7, '秦香秀', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '未发货', '辽宁省辽宁市那个好冷好冷的地方113号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (559, 8, '陈丹', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '辽宁省辽宁市那个好冷好冷的地方114号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (560, 9, '韩政群', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已发货', '辽宁省辽宁市那个好冷好冷的地方115号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (561, 10, '何火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '退款', '辽宁省辽宁市那个好冷好冷的地方116号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (562, 11, '尤琦', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '已收货', '辽宁省辽宁市那个好冷好冷的地方117号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (563, 3, '何婷', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '辽宁省辽宁市那个好冷好冷的地方118号', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (564, 4, '金梓涵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '退款', '天津市天津西站旁xx酒店1楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (565, 5, '卫紫君', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '未发货', '天津市天津西站旁xx酒店2楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (566, 6, '施玉亭', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '天津市天津西站旁xx酒店3楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (567, 7, '华凯敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已发货', '天津市天津西站旁xx酒店4楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (568, 8, '吴融', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '天津市天津西站旁xx酒店5楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (569, 9, '李韵', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '已收货', '天津市天津西站旁xx酒店6楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (570, 10, '金倩', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 15, '未发货', '天津市天津西站旁xx酒店7楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (571, 11, '卫勤', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 79, '退款', '天津市天津西站旁xx酒店8楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (572, 3, '卫显', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 28, '未发货', '天津市天津西站旁xx酒店9楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (573, 4, '漆雕万敏', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 300, '未发货', '天津市天津西站旁xx酒店10楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (574, 5, '喻贞', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 19, '已发货', '天津市天津西站旁xx酒店11楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (575, 6, '朱发弟', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '已收货', '天津市天津西站旁xx酒店12楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (576, 7, '韩凌珍', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 188, '已收货', '天津市天津西站旁xx酒店13楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (577, 8, '施火英', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 68, '未发货', '天津市天津西站旁xx酒店14楼', '无', 1639040053, 1639042349, 0);
INSERT INTO `sp_order` VALUES (578, 9, '张晓磊', 'storage/images/20211224\\49487b04d5474d728ffbea03a3e6abf8.jpeg', 1638878598, '1', 10, '退款', '天津市天津西站旁xx酒店15楼', '无', 1639040053, 1639042349, 0);

-- ----------------------------
-- Table structure for sp_personnel
-- ----------------------------
DROP TABLE IF EXISTS `sp_personnel`;
CREATE TABLE `sp_personnel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工编号',
  `per_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '员工姓名',
  `per_gender` enum('男','女') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '员工性别',
  `per_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '员工照片',
  `per_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '详细地址',
  `per_phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `per_inductiondate` date NOT NULL COMMENT '入职日期',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 42 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_personnel
-- ----------------------------
INSERT INTO `sp_personnel` VALUES (1, '李建成', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道001号', '18277618888', '2021-12-01', '无', 1638876736, 1638876753, 0);
INSERT INTO `sp_personnel` VALUES (2, '王大锤', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道001号', '18277618888', '2021-12-10', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (3, '黄文文', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道001号', '18077640001', '2018-12-01', '无', 1638876736, 1640280431, 0);
INSERT INTO `sp_personnel` VALUES (4, '冯英韶', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道002号', '18077640002', '2018-12-02', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (5, '董伟晔', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道003号', '18077640003', '2018-12-03', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (6, '顾鸿畴', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道004号', '18077640004', '2018-12-04', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (7, '唐鹏鲲', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道005号', '18077640005', '2018-12-05', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (8, '申和畅', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道006号', '18077640006', '2018-12-06', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (9, '夏昕妤', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道007号', '18077640007', '2018-12-07', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (10, '姚乐蓉', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道008号', '18077640008', '2018-12-08', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (11, '姜诗茹', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道009号', '18077640009', '2018-12-09', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (12, '周玉兰', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道010号', '18077640010', '2018-12-10', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (13, '农玉娟', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道011号', '18077640011', '2018-12-11', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (14, '叶菊霞', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道012号', '18077640012', '2018-12-12', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (15, '丁秋露', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道013号', '18077640013', '2018-12-13', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (16, '余思莲', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道014号', '18077640014', '2018-12-14', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (17, '周媛', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道015号', '18077640015', '2018-12-15', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (18, '赵小蝶', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道016号', '18077640016', '2018-12-16', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (19, '陈叶', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道017号', '18077640017', '2018-12-17', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (20, '魏招弟', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道018号', '18077640018', '2018-12-18', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (21, '赵岚', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道019号', '18077640019', '2018-12-19', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (22, '郑阳涌', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道020号', '18077640020', '2018-12-20', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (23, '卫春萍', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道021号', '18077640021', '2018-12-21', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (24, '许雅瑄', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道022号', '18077640022', '2018-12-22', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (25, '尤倩', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道023号', '18077640023', '2018-12-23', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (26, '张山柳', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道024号', '18077640024', '2018-12-24', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (27, '戚滢', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道025号', '18077640025', '2018-12-25', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (28, '施乐之', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道026号', '18077640026', '2018-12-26', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (29, '金平夏', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道027号', '18077640027', '2018-12-27', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (30, '李沛文', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道028号', '18077640028', '2018-12-28', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (31, '尤淑芬', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道029号', '18077640029', '2018-12-29', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (32, '安晓', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道030号', '18077640030', '2018-12-30', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (33, '闾丘纨', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道031号', '18077640031', '2018-12-31', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (34, '钱敏', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道032号', '18077640032', '2019-01-01', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (35, '孙秀丽', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道033号', '18077640033', '2019-01-02', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (36, '赵碧香', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道034号', '18077640034', '2019-01-03', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (37, '杨静', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道035号', '18077640035', '2019-01-04', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (38, '孔丽', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道036号', '18077640036', '2019-01-05', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (39, '韦开萍', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道037号', '18077640037', '2019-01-06', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (40, '施慧', '女', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道038号', '18077640038', '2019-01-07', '无', 1638876736, 1638876736, 0);
INSERT INTO `sp_personnel` VALUES (41, '张德群', '男', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '广西壮族自治区百色市城东区百城街道039号', '18077640039', '2019-01-08', '无', 1638876736, 1638876736, 0);

-- ----------------------------
-- Table structure for sp_product
-- ----------------------------
DROP TABLE IF EXISTS `sp_product`;
CREATE TABLE `sp_product`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '产品编号',
  `pro_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '产品名称',
  `pro_price` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '价格',
  `pro_pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '产品图片',
  `pro_date` date NOT NULL COMMENT '生产日期',
  `per_number` int(11) NOT NULL COMMENT '检验人',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_product
-- ----------------------------
INSERT INTO `sp_product` VALUES (1, '鼠标', '69', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-15', 30, '无', 1639031023, 1640281414, 0);
INSERT INTO `sp_product` VALUES (2, '数据线', '23', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-01', 11, '无', 1639029699, 1639029890, 0);
INSERT INTO `sp_product` VALUES (3, '智能蓝牙小钢炮', '28', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-26', 9, '无', 1639031023, 1639037227, 0);
INSERT INTO `sp_product` VALUES (4, '无息投影机', '300', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-26', 11, '无', 1639031706, 1639031706, 0);
INSERT INTO `sp_product` VALUES (5, '键盘膜', '19', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-09', 11, '无', 1639031706, 1639031706, 0);
INSERT INTO `sp_product` VALUES (6, '雷蛇鼠标', '68', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-04', 11, '无', 1639031706, 1639031706, 0);
INSERT INTO `sp_product` VALUES (7, '狼蛛F2088机械键盘', '188', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-05', 37, '无', 1639031706, 1639037430, 0);
INSERT INTO `sp_product` VALUES (8, '小爱音箱', '68', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-04', 11, '无', 1639031706, 1639031706, 0);
INSERT INTO `sp_product` VALUES (9, '问号OK鸭手机支架', '10', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-07', 11, '无', 1639031706, 1639031706, 0);
INSERT INTO `sp_product` VALUES (10, '磨砂钢化膜', '15', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-05', 11, '无', 1639031706, 1639031706, 0);
INSERT INTO `sp_product` VALUES (11, '蓝牙耳机', '79', 'storage/images/20211224\\2a77df7581a8726058eb3dd92c7a7990.jpeg', '2021-12-05', 11, '无', 1639031706, 1639031706, 0);

-- ----------------------------
-- Table structure for sp_qualityinspection
-- ----------------------------
DROP TABLE IF EXISTS `sp_qualityinspection`;
CREATE TABLE `sp_qualityinspection`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '检验编号',
  `pro_name` int(11) NOT NULL COMMENT '产品名称',
  `qua_date` date NOT NULL COMMENT '检验日期',
  `qua_quality` int(11) NOT NULL COMMENT '质检员',
  `qua_analysis` enum('合格','不合格') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '合格' COMMENT '质量分析',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '修改时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 114 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_qualityinspection
-- ----------------------------
INSERT INTO `sp_qualityinspection` VALUES (1, 3, '2021-12-08', 3, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (2, 11, '2021-12-09', 14, '不合格', '无', 1639059674, 1639059674, 0);
INSERT INTO `sp_qualityinspection` VALUES (3, 1, '2021-10-08', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (4, 2, '2021-10-09', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (5, 3, '2021-10-10', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (6, 4, '2021-10-11', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (7, 5, '2021-10-12', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (8, 6, '2021-10-13', 16, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (9, 7, '2021-10-14', 16, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (10, 8, '2021-10-15', 16, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (11, 9, '2021-10-16', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (12, 10, '2021-10-17', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (13, 11, '2021-10-18', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (14, 1, '2021-10-19', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (15, 2, '2021-10-20', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (16, 3, '2021-10-21', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (17, 4, '2021-10-22', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (18, 5, '2021-10-23', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (19, 6, '2021-10-24', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (20, 7, '2021-10-25', 16, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (21, 8, '2021-10-26', 16, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (22, 9, '2021-10-27', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (23, 10, '2021-10-28', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (24, 11, '2021-10-29', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (25, 1, '2021-10-30', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (26, 2, '2021-10-31', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (27, 3, '2021-11-01', 16, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (28, 4, '2021-11-02', 16, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (29, 5, '2021-11-03', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (30, 6, '2021-11-04', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (31, 7, '2021-11-05', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (32, 8, '2021-11-06', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (33, 9, '2021-11-07', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (34, 10, '2021-11-08', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (35, 11, '2021-11-09', 16, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (36, 1, '2021-11-10', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (37, 2, '2021-11-11', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (38, 3, '2021-11-12', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (39, 4, '2021-11-13', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (40, 5, '2021-11-14', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (41, 6, '2021-11-15', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (42, 7, '2021-11-16', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (43, 8, '2021-11-17', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (44, 9, '2021-11-18', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (45, 10, '2021-11-19', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (46, 11, '2021-11-20', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (47, 1, '2021-11-21', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (48, 2, '2021-11-22', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (49, 3, '2021-11-23', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (50, 4, '2021-11-24', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (51, 5, '2021-11-25', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (52, 6, '2021-11-26', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (53, 7, '2021-11-27', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (54, 8, '2021-11-28', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (55, 9, '2021-11-29', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (56, 10, '2021-11-30', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (57, 11, '2021-12-01', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (58, 1, '2021-12-02', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (59, 2, '2021-12-03', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (60, 3, '2021-12-04', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (61, 4, '2021-12-05', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (62, 5, '2021-12-06', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (63, 6, '2021-12-07', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (64, 7, '2021-12-08', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (65, 8, '2021-12-09', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (66, 9, '2021-12-10', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (67, 10, '2021-12-11', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (68, 11, '2021-12-12', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (69, 1, '2021-12-13', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (70, 2, '2021-12-14', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (71, 3, '2021-12-15', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (72, 4, '2021-12-16', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (73, 5, '2021-12-17', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (74, 6, '2021-12-18', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (75, 7, '2021-12-19', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (76, 8, '2021-12-20', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (77, 9, '2021-12-21', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (78, 10, '2021-12-22', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (79, 11, '2021-12-23', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (80, 1, '2021-12-24', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (81, 2, '2021-12-25', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (82, 3, '2021-12-26', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (83, 4, '2021-12-27', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (84, 5, '2021-12-28', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (85, 6, '2021-12-29', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (86, 7, '2021-12-30', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (87, 8, '2021-12-31', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (88, 9, '2022-01-01', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (89, 10, '2022-01-02', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (90, 11, '2022-01-03', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (91, 1, '2022-01-04', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (92, 2, '2022-01-05', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (93, 3, '2022-01-06', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (94, 4, '2022-01-07', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (95, 5, '2022-01-08', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (96, 6, '2022-01-09', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (97, 7, '2022-01-10', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (98, 8, '2022-01-11', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (99, 9, '2022-01-12', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (100, 10, '2022-01-13', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (101, 11, '2022-01-14', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (102, 1, '2022-01-15', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (103, 2, '2022-01-16', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (104, 3, '2022-01-17', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (105, 4, '2022-01-18', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (106, 5, '2022-01-19', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (107, 6, '2022-01-20', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (108, 7, '2022-01-21', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (109, 8, '2022-01-22', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (110, 9, '2022-01-23', 35, '合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (111, 10, '2022-01-24', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (112, 11, '2022-01-25', 35, '不合格', '无', 1639059674, 1639059846, 0);
INSERT INTO `sp_qualityinspection` VALUES (113, 1, '2022-01-26', 35, '合格', '无', 1639059674, 1639059846, 0);

-- ----------------------------
-- Table structure for sp_workshop
-- ----------------------------
DROP TABLE IF EXISTS `sp_workshop`;
CREATE TABLE `sp_workshop`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '车间编号',
  `wor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '车间名称',
  `wor_admin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '车间管理人员',
  `wor_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '管理人员照片',
  `wor_pic` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '车间照片',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 75 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sp_workshop
-- ----------------------------
INSERT INTO `sp_workshop` VALUES (1, 'SP-GD-001', '李梦曼', '18277648888', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1640279509, 0);
INSERT INTO `sp_workshop` VALUES (2, 'SP-GD-002', '石春琳', '18277648888', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274665, 1639035073, 0);
INSERT INTO `sp_workshop` VALUES (3, 'SP-NN-001', '李建成', '18077640001', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (4, 'SP-NN-002', '宋慕灵', '18077640002', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1640280063, 0);
INSERT INTO `sp_workshop` VALUES (5, 'SP-NN-003', '李梦曼', '18077640003', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (6, 'SP-NN-004', '石春琳', '18077640004', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (7, 'SP-NN-005', '甄好看', '18077640005', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (8, 'SP-NN-006', '萧滢滢', '18077640006', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (9, 'SP-NN-007', '姚慧丽', '18077640007', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (10, 'SP-NN-008', '燕宛海', '18077640008', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (11, 'SP-BS-009', '范思蕊', '18077640009', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (12, 'SP-BS-010', '李建成', '18077640010', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (13, 'SP-BS-011', '宋慕灵', '18077640011', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (14, 'SP-BS-012', '李梦曼', '18077640012', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (15, 'SP-BS-013', '石春琳', '18077640013', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (16, 'SP-BS-014', '甄好看', '18077640014', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (17, 'SP-BS-015', '萧滢滢', '18077640015', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (18, 'SP-BS-016', '姚慧丽', '18077640016', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (19, 'SP-BS-017', '燕宛海', '18077640017', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (20, 'SP-BS-018', '范思蕊', '18077640018', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (21, 'SP-BS-019', '李建成', '18077640019', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (22, 'SP-BS-020', '宋慕灵', '18077640020', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (23, 'SP-GL-021', '李梦曼', '18077640021', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (24, 'SP-GL-022', '石春琳', '18077640022', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (25, 'SP-GL-023', '甄好看', '18077640023', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (26, 'SP-GL-024', '萧滢滢', '18077640024', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (27, 'SP-GL-025', '姚慧丽', '18077640025', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (28, 'SP-GL-026', '燕宛海', '18077640026', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (29, 'SP-GL-027', '范思蕊', '18077640027', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (30, 'SP-GL-028', '李建成', '18077640028', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (31, 'SP-GL-029', '宋慕灵', '18077640029', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (32, 'SP-GL-030', '李梦曼', '18077640030', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (33, 'SP-GL-031', '石春琳', '18077640031', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (34, 'SP-GL-032', '甄好看', '18077640032', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (35, 'SP-GL-033', '萧滢滢', '18077640033', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (36, 'SP-GL-034', '姚慧丽', '18077640034', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (37, 'SP-GL-035', '燕宛海', '18077640035', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);
INSERT INTO `sp_workshop` VALUES (38, 'SP-GL-036', '范思蕊', '18077640035', 'storage/images/20211224\\693aa1e41c00e2abcdf5da149eec329b.jpeg', '无', 1638274057, 1638275619, 0);

-- ----------------------------
-- Table structure for usergroup
-- ----------------------------
DROP TABLE IF EXISTS `usergroup`;
CREATE TABLE `usergroup`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户编号',
  `group_id` int(11) NOT NULL DEFAULT 0 COMMENT '分组编号',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态(0:禁用,1:启用)',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `delete_time` int(11) NOT NULL DEFAULT 0 COMMENT '删除时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户分组表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of usergroup
-- ----------------------------
INSERT INTO `usergroup` VALUES (1, 1, 1, 1, 1637067993, 1637067993, 0);
INSERT INTO `usergroup` VALUES (2, 2, 2, 1, 1637067993, 1638271852, 1638271852);
INSERT INTO `usergroup` VALUES (3, 1, 1, 1, 1638271860, 1638271860, 0);

-- ----------------------------
-- View structure for view_employee_sums
-- ----------------------------
DROP VIEW IF EXISTS `view_employee_sums`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `view_employee_sums` AS select month(date_format(from_unixtime(`sp_employeesign`.`emp_date`),'%Y-%m-%d %H:%i:%s')) AS `emp_mm`,count(((`sp_employeesign`.`emp_status` = '正常签到') or NULL)) AS `emp_signin`,count(((`sp_employeesign`.`emp_status` = '未签到') or NULL)) AS `emp_nosign`,count(((`sp_employeesign`.`emp_status` = '缺勤') or NULL)) AS `emp_leave`,count(((`sp_employeesign`.`emp_status` = '请假') or NULL)) AS `emp_absent` from `sp_employeesign` group by `emp_mm`;

-- ----------------------------
-- View structure for view_fau_sums
-- ----------------------------
DROP VIEW IF EXISTS `view_fau_sums`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `view_fau_sums` AS select year(`sp_fault`.`fau_date`) AS `yy`,month(`sp_fault`.`fau_date`) AS `mm`,count(0) AS `nums` from `sp_fault` group by `yy`,`mm`;

-- ----------------------------
-- View structure for view_ordprice_sums
-- ----------------------------
DROP VIEW IF EXISTS `view_ordprice_sums`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `view_ordprice_sums` AS select sum(`sp_order`.`ord_price`) AS `sums`,`sp_order`.`pro_name` AS `ord_name`,`sp_product`.`pro_name` AS `pro_name`,`sp_order`.`ord_price` AS `ord_price` from (`sp_order` left join `sp_product` on((`sp_order`.`pro_name` = `sp_product`.`id`))) group by `sp_order`.`pro_name`,`sp_order`.`ord_price`;

-- ----------------------------
-- View structure for view_ordstatus_sums
-- ----------------------------
DROP VIEW IF EXISTS `view_ordstatus_sums`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `view_ordstatus_sums` AS select substr(`sp_order`.`ord_status`,1,3) AS `status`,count(0) AS `nums` from `sp_order` group by substr(`sp_order`.`ord_status`,1,3);

-- ----------------------------
-- View structure for view_per_sums
-- ----------------------------
DROP VIEW IF EXISTS `view_per_sums`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `view_per_sums` AS select substr(`sp_order`.`ord_address`,1,2) AS `province`,count(0) AS `nums` from `sp_order` group by substr(`sp_order`.`ord_address`,1,2);

-- ----------------------------
-- View structure for view_pro_sums
-- ----------------------------
DROP VIEW IF EXISTS `view_pro_sums`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `view_pro_sums` AS select count(0) AS `nums`,`sp_order`.`pro_name` AS `ord_name`,`sp_product`.`pro_name` AS `pro_name` from (`sp_order` left join `sp_product` on((`sp_order`.`pro_name` = `sp_product`.`id`))) group by `sp_order`.`pro_name`;

-- ----------------------------
-- Procedure structure for getPids
-- ----------------------------
DROP PROCEDURE IF EXISTS `getPids`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getPids`(in  ids varchar(500),out  menu_ids varchar(500))
BEGIN
	declare done int default 0;  
	declare hostId bigint; 

	
	
  declare idCur cursor for select id from menu where id in (
		SELECT  DISTINCT SUBSTRING_INDEX(SUBSTRING_INDEX(ids,',',help_topic_id+1),',',-1) AS id FROM  mysql.help_topic 
	);
  
  declare continue handler for not FOUND set done = 1; 
	 drop TEMPORARY TABLE if exists tmp_table;
	 CREATE TEMPORARY TABLE tmp_table (id int NOT NULL);
	open idCur;  
	
	
	REPEAT
	fetch idCur into hostId;  
	if not done THEN  
	
			insert into tmp_table select * from (
			SELECT T2.id FROM ( 
					SELECT  @r AS _id, (SELECT @r := pid FROM menu WHERE id = _id) AS pid,  @l := @l + 1 AS lvl FROM  
					(SELECT @r := hostId, @l := 0) vars, menu h  WHERE @r <> 0
			) T1	
			JOIN menu T2 ON T1._id = T2.id) a;
			
	end if;
	until done end repeat;
	close idCur;
	
	set menu_ids=(SELECT GROUP_CONCAT(id) as menu_ids FROM (select DISTINCT * from tmp_table order by id) a);
	 

	drop TEMPORARY TABLE tmp_table;
	
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
