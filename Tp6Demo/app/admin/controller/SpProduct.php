<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\db\exception\DbException;
use think\facade\Db;
use think\Request;
use think\response\View;

class SpProduct extends Base
{
    /**
     * 显示资源列表
     *
     * @return array|View
     * @throws DbException
     */
    public function index()
    {

        if(request()->isPost()){
            return \app\admin\model\SpProduct::getList(input('post.'));
        }
        return view();//显示界面
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        if(request()->isPost()){
            $data = input('post.');
            return \app\admin\model\SpProduct::saveAdd($data);
        }
        $perList = \app\admin\model\SpPersonnel::select();
        \think\facade\View::assign('perList',$perList);
        return view();//显示界面
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        if(request()->isPost()){
            return \app\admin\model\SpProduct::edit(input('post.'));
        }
        $row = \app\admin\model\SpProduct::getRow($id);
        \think\facade\View::assign('row',$row);//将数据传输到页面

        $perList = \app\admin\model\SpPersonnel::select();
        \think\facade\View::assign('perList',$perList);
        return view();//显示页面
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        return \app\admin\model\SpProduct::del($id);
    }
}
