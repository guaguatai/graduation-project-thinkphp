<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\db\exception\DbException;
use think\facade\View;
use think\Request;
use think\response\Json;

class Group extends Base
{
    /**
     * 显示资源列表
     *
     * @return \think\response\View
     * @throws DbException
     */
    public function index()
    {

        if (request()->isPost()) {
            return \app\admin\model\Group::getList(input('post.'));
        }
        return view();//显示界面
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        if (request()->isPost()) {
            $data = input('post.');
            return \app\admin\model\Group::saveAdd($data);
        }
        return view();//显示界面
    }

    /**
     * 显示指定的资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        if (request()->isPost()) {
            return \app\admin\model\Group::edit(input('post.'));
        }
        $row = \app\admin\model\Group::getRow($id);
        View::assign('row', $row);//将数据传输到页面
        return view();//显示页面
    }


    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        return \app\admin\model\Group::del($id);
    }

    /**
     * 保存分组菜单权限
     *
     * @param Request $request
     * @return Json|\think\response\View
     */
    public function auth(Request $request)
    {
        if($request->isPost()){
            $data = $request->post();
            return \app\admin\model\Group::savaAuth($data);
        }
        $id = $request->get('id',0);
        $row = \app\admin\model\Group::field('id,menu_ids')->find($id);
        View::assign('row',$row);
        return view();
    }


    /**
     * 获取菜单的树结构数据
     * @return array
     */
    public function getAuth(){
        return \app\admin\model\Menu::getMenuTree();
    }

}
