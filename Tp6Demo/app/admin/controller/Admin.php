<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\db\exception\DbException;
use think\facade\View;
use think\Request;
use think\response\Json;

class Admin extends Base
{
    /**
     * 显示资源列表
     * @return array|\think\response\View
     * @throws DbException
     */
    public function index()
    {

        if(request()->isPost()){
            return \app\admin\model\Admin::getList(input('post.'));
        }
        return view();//显示界面
    }


    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        if(request()->isPost()){
            $data = input('post.');
            return \app\admin\model\Admin::saveAdd($data);
        }
        return view();//显示页面
    }


    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit(int $id)
    {
        if(request()->isPost()){
            return \app\admin\model\Admin::edit(input('post.'));
        }
        $row = \app\admin\model\Admin::getRow($id);
        View::assign('row',$row);//将数据传输到页面
        return view();//显示页面
    }


    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        return \app\admin\model\Admin::del($id);
    }

    /**
     * 设置基础资料
     */
    public function baseinfo(){
        //判断请求的方式是否为post
        if(request()->isPost()){
            //用户已经提交数据过来
            $data = input('post.');//一次性接收用户提交的所有数据（数组）
            return \app\admin\model\Admin::baseinfo($data);//将数据完全交给模型处理
        }

        if(!session('?user')) return redirect((string)url('admin/index/login'));
        //获取当前用户的基本资料
        $row = \app\admin\model\Admin::find(1);
        View::assign('info',$row);//将用户数据以变量名为$info的方式传给页面
        return view();//显示页面
    }


    public function editpsw(){
        //要判断当前是否为post方式提交,request:表示一个请求
        if(request()->isPost()){
            //验证旧密码，修改密码
            $data = input('post.');//一次性获取所有用户提交的信息
            return \app\admin\model\Admin::editpsw($data);//将密码数据交给模型（Admin）处理
        }

        return view();//显示页面
    }

}
