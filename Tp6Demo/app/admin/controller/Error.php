<?php
declare (strict_types = 1);

namespace app\admin\controller;
/**
 * 错误处理
 * Class Error
 * @package app\admin\controller
 */
class Error
{
    /**
     * 打开404页面
     * @return \think\response\View
     */
    function err_404(){
        return view();//显示页面
    }
}
