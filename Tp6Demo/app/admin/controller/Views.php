<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\db\exception\DbException;
use think\facade\Db;
use think\Request;
use think\response\View;

class Views extends Base
{
    /**
     * 显示资源列表
     *
     * @return array|View
     * @throws DbException
     */
    public function index()
    {
        return view();//显示界面
    }

    /**
     * 员工签到状态统计
     * @return \think\response\Json
     */
    public function view_employee_sums(){
        $list = Db::query("select * from view_employee_sums");
        return json($list);
    }

    /**
     * 本年度故障状态统计
     * @return \think\response\Json
     */
    public function getfau_sums(){
        $list = Db::query("select * from view_fau_sums");
        return json($list);
    }

    /**
     * 订单表各类产品销售额统计
     * @return \think\response\Json
     */
    public function view_ordprice_sums(){
        $list = Db::query("select * from view_ordprice_sums");
        return json($list);
    }

    /**
     * 订单表订单状态统计
     * @return \think\response\Json
     */
    public function view_ordstatus_sums(){
        $list = Db::query("select * from view_ordstatus_sums");
        return json($list);
    }

    /**
     * 顾客分布地区统计
     * @return \think\response\Json
     */
    public function view_per_sums(){
        $list = Db::query("select * from view_per_sums");
        return json($list);
    }

    /**
     * 各类产品销售数量统计
     * @return \think\response\Json
     */
    public function view_pro_sums(){
        $list = Db::query("select * from view_pro_sums");
        return json($list);
    }

    /**
     * 订单总量
     * @return \think\response\Json
     */
    public function sp_order(){
        $list = Db::query("select * from sp_order where delete_time = '0'");
        return json($list);
    }

    /**
     * 故障总量
     * @return \think\response\Json
     */
    public function sp_fault(){
        $list = Db::query("select * from sp_fault where delete_time = '0'");
        return json($list);
    }
}
