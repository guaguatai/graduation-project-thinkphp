<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\db\exception\DbException;
use think\Request;
use think\response\View;

class Usergroup extends Base
{
    /**
     * 显示资源列表
     *
     * @return array|View
     * @throws DbException
     */
    public function index()
    {
        if(request()->isPost()){
            return \app\admin\model\Usergroup::getList(input('post.'));
        }
        return view();//显示界面
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        if(request()->isPost()){
            $data = input('post.');
            return \app\admin\model\Usergroup::saveAdd($data);
        }
        $userList = \app\admin\model\Admin::where('status',1)->select();
        $groupList = \app\admin\model\Group::where('status',1)->select();
        \think\facade\View::assign('userList',$userList);
        \think\facade\View::assign('groupList',$groupList);
        return view();//显示界面
    }


    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        if(request()->isPost()){
            return \app\admin\model\Usergroup::edit(input('post.'));
        }
        $row = \app\admin\model\Usergroup::getRow($id);
        \think\facade\View::assign('row',$row);//将数据传输到页面
        $userList = \app\admin\model\Admin::where('status',1)->select();
        $groupList = \app\admin\model\Group::where('status',1)->select();
        \think\facade\View::assign('userList',$userList);
        \think\facade\View::assign('groupList',$groupList);
        return view();//显示页面
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        return \app\admin\model\Usergroup::del($id);
    }
}
