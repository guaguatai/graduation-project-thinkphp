<?php
declare (strict_types = 1);

namespace app\admin\controller;


use think\facade\Filesystem;

class Uploader
{
//    上传图片
    function uploadImg(){
        $file = request()->file('file');
//        上传到本地服务器
        $savename = \think\facade\Filesystem::disk('public')->putFile('images',$file);
        $data = [
            'code' => '0',
            'msg' => '',
            'data' => [
                'src' => 'storage/'.$savename,
                'title' => ''
            ]
        ];
        return json($data);
    }
}
