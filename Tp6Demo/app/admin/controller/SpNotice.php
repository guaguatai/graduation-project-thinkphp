<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\db\exception\DbException;
use think\facade\Db;
use think\Request;
use think\response\View;

class SpNotice extends Base
{
    /**
     * 显示资源列表
     *
     * @return array|View
     * @throws DbException
     */
    public function index()
    {

        if(request()->isPost()){
            return \app\admin\model\SpNotice::getList(input('post.'));
        }
        return view();//显示界面
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        if(request()->isPost()){
            $data = input('post.');
            return \app\admin\model\SpNotice::saveAdd($data);
        }
        return view();//显示界面
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        if(request()->isPost()){
            return \app\admin\model\SpNotice::edit(input('post.'));
        }
        $row = \app\admin\model\SpNotice::getRow($id);
        $row->not_time = date("Y-m-d H:i:s", $row->not_time);
        \think\facade\View::assign('row',$row);//将数据传输到页面
        return view();//显示页面
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        return \app\admin\model\SpNotice::del($id);
    }
}
