<?php
declare (strict_types = 1);

namespace app\admin\controller;

use think\db\exception\DbException;
use think\facade\Db;
use think\Request;
use think\response\View;

class Menu extends Base
{
    /**
     * 显示资源列表
     *
     * @return array|View
     * @throws DbException
     */
    public function index()
    {

        if(request()->isPost()){
            return \app\admin\model\Menu::getList(input('post.'));
        }
        return view();//显示界面
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        if(request()->isPost()){
            $data = input('post.');
            return \app\admin\model\Menu::saveAdd($data);
        }
        return view();//显示界面
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        //
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        if(request()->isPost()){
            return \app\admin\model\Menu::edit(input('post.'));
        }
        $row = \app\admin\model\Menu::getRow($id);
        \think\facade\View::assign('row',$row);//将数据传输到页面
        return view();//显示页面
    }


    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        return \app\admin\model\Menu::del($id);
    }

    // 获取初始化数据
    public function init(){
        $homeInfo = [
            'title' => '首页',
            'href'  => (string)url('admin/index/welcome'),
        ];
        $logoInfo = [
            'title' => '智能车间管理',
            'image' => '/static/layuimini/images/logo.png',
        ];
        $menuInfo = \app\admin\model\Menu::getMenuList();
        $systemInit = [
            'homeInfo' => $homeInfo,
            'logoInfo' => $logoInfo,
            'menuInfo' => $menuInfo,
        ];
        return json($systemInit);
    }


    // 获取初始化数据
    public function getTreeList(){
        if(request()->isPost()) {
            $treeList = \app\admin\model\Menu::getMenuTreeList();
            return json($treeList);
        }
        return info(false,"非法提交！");
    }

}
