<?php
declare (strict_types = 1);

namespace app\admin\controller;

use app\admin\model\Admin;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Db;
use think\facade\Session;
use think\response\Json;
use think\response\Redirect;
use think\response\View;

class Index extends Base
{
    /**
     * admin应用的主界面
     * @return string
     *
     */
    public function index()
    {
        //在显示主界面前，验证用户是否已登录
        if(!session('?user')){
            //用户未登录，跳转到登录界面(重定向)
            /*return '<script>
                        window.location.href = "/admin/index/login";
                    </script>';*/
            return redirect((string)url('/admin/index/login'));
        }

        return view();//显示主界面
    }

    /**
     * 欢迎界面
     */
    public function welcome()
    {
        if(!session('?user')){
            return redirect((string)url('/admin/index/login'));
        }
        return view();
    }

    /**
     * admin应用的登录界面
     * @return Json|View
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function login()
    {
        if(request()->isPost()){
           //验证账号、密码、验证码
            /*$user = input('post.user');
            $psw = input('post.psw');
            $code = input('post.code');
            halt($user,$psw,$code);*/
            $data = input('post.');

            //if(!captcha_check($data['code'])) return info(false,'验证码错误！');

            //::表示静态调用方法
            return Admin::login($data);

        }
        //清空session中保存的用户信息
        Session::clear();

        //dump(password_hash('123456',PASSWORD_DEFAULT));

        return view();
    }


    /**
     * 退出系统
     * @return Json
     */
    public function logout()
    {
        //清空session
        Session::clear();
        return info(true,'成功退出系统！');
    }
}
