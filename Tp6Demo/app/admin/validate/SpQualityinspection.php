<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpQualityinspection extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'                =>'require|integer',
        'pro_name'          =>'require',
        'qua_date'          =>'require',
        'qua_quality'       =>'require',
        'qua_analysis'      =>'require',
        'remark'            =>'require'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'                    =>'编号不能为空！',
        'id.integer'                    =>'编号格式有误！',
        'pro_name.require'              =>'产品名称不能为空！',
        'qua_date.require'              =>'检验日期不能为空！',
        'qua_quality.require'           =>'质检员不能为空！',
        'qua_analysis.require'          =>'质量分析不能为空！',

    ];

    /**
     * 添加数据的验证场景
     * @return SpQualityinspection
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "pro_name","qua_date","qua_quality","qua_analysis"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpQualityinspection
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpQualityinspection
     */
    public function sceneEdit()
    {
        return $this->only([
            "pro_name","qua_date","qua_quality","qua_analysis"
        ]);
    }
}
