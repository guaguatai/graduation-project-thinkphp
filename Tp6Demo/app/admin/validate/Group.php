<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Group extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'            =>  'require|integer',
        'gname'         =>  'require',
        'sort'          =>  'require|integer',
        'status'        =>  'require|in:0,1',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'            =>'编号不能为空！',
        'id.integer'            =>'编号格式有误！',
        'gname.require'         =>'菜单名称不能为空！',
        'sort.require'          =>'排序值不能为空！',
        'sort.integer'          =>'排序值格式有误！',
        'status.require'        =>'状态不能为空！',
        'status.in'             =>'状态格式有误！',
    ];

    /**
     * 添加数据的验证场景（saveadd）
     * @return Group
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "gname","sort","status"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return Group
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return Group
     */
    public function sceneEdit()
    {
        return $this->only([
            "id","gname","sort","status"
        ]);
    }
}
