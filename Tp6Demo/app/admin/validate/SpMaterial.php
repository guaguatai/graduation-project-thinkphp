<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpMaterial extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'                =>'require|integer',
        'mat_name'          =>'require',
        'mat_quantity'      =>'require',
        'mat_pic'           =>'require',
        'mat_date'          =>'require',
        'mat_per'           =>'require',
        'remark'            =>'require'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'                    =>'编号不能为空！',
        'id.integer'                    =>'编号格式有误！',
        'mat_name.require'              =>'物料名称不能为空！',
        'mat_quantity.require'          =>'数量不能为空！',
        'mat_date.require'              =>'采购日期不能为空！',
        'mat_per.require'               =>'采购人不能为空！'

    ];

    /**
     * 添加数据的验证场景
     * @return SpMaterial
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "mat_name","mat_quantity","mat_date","mat_per"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpMaterial
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpMaterial
     */
    public function sceneEdit()
    {
        return $this->only([
            "mat_name","mat_quantity","mat_date","mat_per"
        ]);
    }
}
