<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpProduct extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'                =>'require|integer',
        'pro_name'          =>'require',
        'pro_pic'           =>'require',
        'pro_date'          =>'require',
        'per_number'        =>'require',
        'remark'            =>'require'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'                    =>'编号不能为空！',
        'id.integer'                    =>'编号格式有误！',
        'pro_name.require'              =>'产品名称不能为空！',
        'pro_date.require'              =>'生产日期不能为空！',
        'per_number.require'            =>'产品检验人名称不能为空！',

    ];

    /**
     * 添加数据的验证场景
     * @return SpProduct
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "pro_name","pro_date","per_number"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpProduct
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpProduct
     */
    public function sceneEdit()
    {
        return $this->only([
            "pro_name","pro_date","per_number"
        ]);
    }
}
