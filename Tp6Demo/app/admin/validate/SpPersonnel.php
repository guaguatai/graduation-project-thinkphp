<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpPersonnel extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'                     =>'require|integer',
        'per_name'               =>'require',
        'per_gender'             =>'require',
        'per_pic'                =>'require',
        'per_address'            =>'require',
        'per_phone'              =>'require',
        'per_inductiondate'      =>'require'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'                    =>'编号不能为空！',
        'id.integer'                    =>'编号格式有误！',
        'per_name.require'              =>'员工姓名不能为空！',
        'per_phone.require'             =>'员工手机号不能为空！',
        'per_inductiondate.require'     =>'员工入职日期不能为空！',


    ];

    /**
     * 添加数据的验证场景
     * @return SpPersonnel
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "per_name","per_phone","per_inductiondate"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpPersonnel
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpPersonnel
     */
    public function sceneEdit()
    {
        return $this->only([
            "per_name","per_phone","per_inductiondate"
        ]);
    }
}
