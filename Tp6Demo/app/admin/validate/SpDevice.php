<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpDevice extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'                =>'require|integer',
        'dev_name'          =>'require',
        'dev_date'          =>'require',
        'dev_manufacturer'  =>'require',
        'dev_type'          =>'require',
        'dev_status'        =>'require',
        'remark'            =>'require'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'                    =>'编号不能为空！',
        'id.integer'                    =>'编号格式有误！',
        'dev_name.require'              =>'设备名称不能为空！',
        'dev_date.require'              =>'设备生产日期不能为空！',
        'dev_manufacturer.require'      =>'设备名称不能为空！',
        'dev_type.require'              =>'设备类型不能为空！',
        'dev_status.require'            =>'设备状态不能为空！',

    ];

    /**
     * 添加数据的验证场景
     * @return SpDevice
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "dev_name","dev_date","dev_manufacturer","dev_type","dev_status"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpDevice
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpDevice
     */
    public function sceneEdit()
    {
        return $this->only([
            "dev_name","dev_date","dev_manufacturer","dev_type","dev_status"
        ]);
    }
}
