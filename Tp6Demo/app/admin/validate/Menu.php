<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Menu extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'            =>'require|integer',
        'pid'           =>'require|integer',
        'title'         =>'require',
        'sort'          =>'require|integer',
        'status'        =>'require|in:0,1',
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'            =>'编号不能为空！',
        'id.integer'            =>'编号格式有误！',
        'pid.require'           =>'父ID不能为空！',
        'pid.integer'           =>'父ID格式有误！',
        'title.require'         =>'菜单名称不能为空！',
        'sort.require'          =>'排序值不能为空！',
        'sort.integer'          =>'排序值格式有误！',
        'status.require'        =>'状态不能为空！',
        'status.in'             =>'状态格式有误！',
    ];

    /**
     * 添加数据的验证场景（saveadd）
     * @return Menu
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "pid","title","sort","status"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return Menu
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return Menu
     */
    public function sceneEdit()
    {
        return $this->only([
            "id","pid","title","sort","status"
        ]);
    }
}
