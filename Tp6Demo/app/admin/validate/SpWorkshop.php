<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpWorkshop extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'            =>'require|integer',
        'wor_name'      =>'require',
        'wor_admin'     =>'require',
        'wor_phone'     =>'require',
        'wor_pic'       =>'require',
        'remark'        =>'require'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'            =>'编号不能为空！',
        'id.integer'            =>'编号格式有误！',
        'wor_name.require'      =>'车间名称不能为空！',
        'wor_admin.require'     =>'车间管理人员不能为空！',
        'wor_phone.require'     =>'管理人员手机号不能为空！',
    ];

    /**
     * 添加数据的验证场景
     * @return SpWorkshop
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "wor_name","wor_admin","wor_phone"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpWorkshop
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpWorkshop
     */
    public function sceneEdit()
    {
        return $this->only([
            "wor_name","wor_admin","wor_phone"
        ]);
    }
}
