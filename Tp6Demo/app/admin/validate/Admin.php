<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Admin extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'            =>'require|integer',
        'user'          =>'require|length:6,50|alphaNum',
        'phone'         =>'mobile',
        'email'         =>'email',
        'full_name'     =>'require|chs|min:2',
        'psw'           =>'require|alphaNum|length:8,200',
        'comf_psw'      =>'confirm:psw',
        'login_num'     =>'require|integer',
        'sort'          =>'require|integer',
        'status'        =>'require|in:0,1',
        'code'          => 'require|captcha',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'    => '编号不能为空！',
        'id.integer'    => '编号只能是整数！',
        'user.require'  => '请输入用户名',
        'user.alphaNum' => '用户名只能为字母或数字',
        'psw.require'   => '请输入密码',
        'psw.length'    => '密码的长度为8-200！',
        'code.require'  => '请输入验证码',
        'code.captcha'  => '验证码错误',
    ];

    /**
     * 验证场景
     * @var \string[][]
     */
    /*protected $scene = [
        'login'  =>  ['user','psw','code'], //登录场景，只验证用户名、密码、验证码
        'edit'   =>  ['psw','name'], //修改信息场景
    ];*/

    /**
     * 登录的验证场景(login)
     * 方法名必须要有“scene”作为前缀
     * 场景名称首字母大写，但是在使用的时候去掉“scene”前缀全小写
     * @return Admin
     */
    public function sceneLogin()
    {
        return $this->only(['user','psw','code'])
            ->remove('user','length')//登录时不验证用户名长度
            ->remove('psw','length|alphaNum');
    }

    /**
     * 保存基本资料的验证场景(baseinfo)
     * 方法名必须要有“scene”作为前缀
     * 场景名称首字母大写，但是在使用的时候去掉“scene”前缀全小写
     * @return Admin
     */
    public function sceneBaseinfo(){
        return  $this->only(['id','full_name','phone','email']);
    }

    /**
     * 添加数据的验证场景（saveadd）
     * @return Admin
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "user","phone","email","full_name","psw","comf_psw","status"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return Admin
     */
    public function sceneId(){
        return $this->only(["id"]);
    }


    /**
     * 修改数据的验证场景（edit）
     * @return Admin
     */
    public function sceneEdit()
    {
        return $this->only([
            "id","phone","email",
            "full_name","psw",
            "comf_psw","status"
        ])->remove('psw','require');
    }
}
