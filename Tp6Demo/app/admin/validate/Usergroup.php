<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class Usergroup extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'            =>  'require|integer',
        'user_id'         =>  'require|integer',
        'group_id'          =>  'require|integer',
        'status'        =>  'require|in:0,1',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'            =>'编号不能为空！',
        'id.integer'            =>'编号格式有误！',
        'user_id.require'         =>'用户编号不能为空！',
        'user_id.integer'            =>'用户编号格式有误！',
        'group_id.require'         =>'分组编号不能为空！',
        'group_id.integer'            =>'分组编号格式有误！',
        'status.require'        =>'状态不能为空！',
        'status.in'             =>'状态格式有误！',
    ];

    /**
     * 添加数据的验证场景（saveadd）
     * @return Usergroup
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "user_id","group_id","status"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return Usergroup
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return Usergroup
     */
    public function sceneEdit()
    {
        return $this->only([
            "id","user_id","group_id","status"
        ]);
    }
}
