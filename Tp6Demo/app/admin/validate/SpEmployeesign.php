<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpEmployeesign extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'                =>'require|integer',
        'emp_name'          =>'require',
        'emp_date'          =>'require',
        'emp_status'        =>'require',
        'emp_pic'           =>'require',
        'remark'            =>'require'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'                    =>'编号不能为空！',
        'id.integer'                    =>'编号格式有误！',
        'emp_name.require'              =>'员工姓名不能为空！',
        'emp_date.require'              =>'签到时间不能为空！',
        'emp_status.require'            =>'签到状态不能为空！',


    ];

    /**
     * 添加数据的验证场景
     * @return SpEmployeesign
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "emp_name","emp_date","emp_status"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpEmployeesign
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpEmployeesign
     */
    public function sceneEdit()
    {
        return $this->only([
            "emp_name","emp_date","emp_status"
        ]);
    }
}
