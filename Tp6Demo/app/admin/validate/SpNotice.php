<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpNotice extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'                 =>'require|integer',
        'not_title'          =>'require',
        'not_con'            =>'require',
        'not_time'           =>'require',
        'not_pic'            =>'require',
        'remark'             =>'require',
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'                    =>'编号不能为空！',
        'id.integer'                    =>'编号格式有误！',
        'not_title.require'             =>'公告标题不能为空！',
        'not_con.require'               =>'公告内容不能为空！',
        'not_time.require'              =>'设发布时间不能为空！',

    ];

    /**
     * 添加数据的验证场景
     * @return SpNotice
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "not_title","not_con","not_time"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpNotice
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpNotice
     */
    public function sceneEdit()
    {
        return $this->only([
            "not_title","not_con","not_time"
        ]);
    }
}
