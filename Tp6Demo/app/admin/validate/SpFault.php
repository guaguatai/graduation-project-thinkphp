<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpFault extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'                =>'require|integer',
        'fau_dev'           =>'require',
        'fau_pic'           =>'require',
        'fau_date'          =>'require',
        'fau_reason'        =>'require',
        'per_jobnumber'     =>'require',
        'fau_status'        =>'require',
        'remark'            =>'require'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'                    =>'编号不能为空！',
        'id.integer'                    =>'编号格式有误！',
        'fau_dev.require'               =>'设备名称不能为空！',
        'fau_date.require'              =>'故障日期不能为空！',
        'fau_reason.require'            =>'故障原因不能为空！',
        'per_jobnumber.require'         =>'故障核实人员不能为空！',
        'fau_status.require'            =>'故障状态不能为空！',

    ];

    /**
     * 添加数据的验证场景
     * @return SpFault
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "fau_dev","fau_date","fau_reason","per_jobnumber","fau_status"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpFault
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpFault
     */
    public function sceneEdit()
    {
        return $this->only([
            "fau_dev","fau_date","fau_reason","per_jobnumber","fau_status"
        ]);
    }
}
