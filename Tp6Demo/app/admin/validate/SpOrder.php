<?php
declare (strict_types = 1);

namespace app\admin\validate;

use think\Validate;

class SpOrder extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'id'                =>'require|integer',
        'pro_name'          =>'require',
        'ord_buyers'        =>'require',
        'ord_pic'           =>'require',
        'ord_date'          =>'require',
        'ord_quantity'      =>'require',
        'ord_price'         =>'require',
        'ord_status'        =>'require',
        'ord_address'       =>'require',
        'remark'            =>'require'
    ];


    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'id.require'                    =>'编号不能为空！',
        'id.integer'                    =>'编号格式有误！',
        'pro_name.require'              =>'产品名称不能为空！',
        'ord_buyers.require'            =>'买家姓名不能为空！',
        'ord_quantity.require'          =>'数量数量不能为空！',
        'ord_price.require'             =>'订单价格不能为空！',
        'ord_status.require'            =>'订单状态不能为空！',
        'ord_address.require'           =>'收货地址不能为空！',

    ];

    /**
     * 添加数据的验证场景
     * @return SpOrder
     */
    public function sceneSaveadd()
    {
        return $this->only([
            "pro_name","ord_buyers","ord_quantity","ord_price","ord_status","ord_address"
        ]);
    }

    /**
     * 验证编号的场景（id）
     * @return SpOrder
     */
    public function sceneId(){
        return $this->only(["id"]);
    }

    /**
     * 修改数据的验证场景（edit）
     * @return SpOrder
     */
    public function sceneEdit()
    {
        return $this->only([
            "pro_name","ord_buyers","ord_quantity","ord_price","ord_status","ord_address"
        ]);
    }
}
