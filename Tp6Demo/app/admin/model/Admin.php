<?php


namespace app\admin\model;


use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\exception\ValidateException; //验证器的异常处理类
use think\facade\Db;
use think\helper\Str;
use think\Model;
use think\response\Json;

class Admin extends BaseMode
{
    /**
     * 验证登录
     * @param $data array 用户输入胡数据
     * @return Json
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function login($data){
        //数据合法性验证
        try {
            //一次性验证所有信息，包括：用户名、密码、验证码
            //如果使用验证场景，验证前必须先调用相应的场景
            validate(\app\admin\validate\Admin::class)
                ->scene('login')
                ->check($data);
        } catch (ValidateException $e) {
            // 验证失败 返回错误信息
            return info(false, $e->getError());
        }

        //从数据库读取用户信息，并进行验证
        //select：获取所有符合条件的数据
        //find: 获取符合条件的一行数据，没有数据则为null
        //where:查询条件，默认用=
        //$user = Db::name('admin')->where('user',$data['user'])->find();
        //self：模型本身，第一人称
        $user = self::where('user',$data['user'])->find();
        if(!$user) return info(false,'用户名不存在');
        //哈希密码特有的验证方式：password_verify(用户输入在密码,数据的加密密码)
        if(!password_verify($data['psw'].$user['psw_salt'],$user['psw']))
            return info(false,'账号密码不匹配');
        //登录成功后，保存用户信息：id,user,name
        session('user_id',$user['id']);
        session('user',$user['user']);
        session('full_name',$user['full_name']);


        return info(true,'登录成功');

    }

    /**
     * 保存基本资料
     * @param $data
     * @return Json
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function baseinfo($data)
    {
        try {
            validate(\app\admin\validate\Admin::class)
                ->scene('baseinfo')
                ->check($data);
        } catch (ValidateException $e){
            return info(false,$e->getError());
        }

        $row = self::find($data['id']);//先查询到数据，再进行修改
        if(!$row) return info(false,'用户不存在');
        //更新查询到的信息
        $row->full_name = $data['full_name'];
        $row->phone = $data['phone'];
        $row->email = $data['email'];
        $row->remark = $data['remark'];
        //保存到数据库
        if($row->save()) return info(true,'保存成功');
        return info(false,'保存失败');
    }

    /**
     * 修改密码
     * @param $data
     * @return Json
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function editpsw($data)
    {
        //获取数据库中密码，并且与用户输入的旧密码进行比较
        $row = self::find(session('user_id'));
        if(!password_verify($data['old_psw'].$row->psw_salt,$row->psw)){
            //如果两个密码相等，说明密码是对的
            //判断两次新密码是否一致
            if($data['new_psw'] != $data['again_psw']){
                return info(false,'两次输入的新密码不一致');
            }
            $psw_salt = Str::random(8);//随机获取密码盐

            $row->psw_salt = $psw_salt;//更新密码盐
            $row->psw = password_hash($data['new_psw'].$psw_salt,PASSWORD_DEFAULT);//更新密码
            if($row->save())  return info(true,'密码修改成功');
            return info(false,'密码修改失败');
        }

        return info(false,'旧密码错误');
    }

    /**
     * 获取数据列表（带分页）
     * @param $parm
     * @return array
     * @throws DbException
     */
    public static function getList($parm){
        $where = self::initParams($parm);
        $list = self::order('id','asc')
            ->where($where)
            ->paginate($parm['limit'])
            ->each(function ($item, $key){
                $item['status'] = $item['status']==1?'启用':'禁用';
                return $item;
            });//根据编号倒序排序，每页显示10行
        return [
            'code'  => 0,  //不用修改
            'msg'   => '', //不用修改
            'count' => $list->total(), //获取数据库表中的数据总数
            'data'  => $list->items() //获取当前页面数据
        ];
    }

    /**
     * 删除
     * @param $id
     * @return Json
     */
    public static function del($id)
    {
       if(self::destroy($id)) return info(true,"删除成功");
       return info(true,"删除失败");

    }

    /**
     * 保存添加的数据
     * @param array $data
     * @return Json
     */
    public static function saveAdd(array $data)
    {
        //验证数据的合法性
        try {
            validate(\app\admin\validate\Admin::class)
                ->scene('saveadd')
                ->check($data);
        }
        catch (ValidateException $e){
            return info(false,$e->getError());//返回验证的错误信息
        }
        //保存数据到数据库
        //生成密码盐，加密密码
        $data['psw_salt'] = Str::random(8);
        $data['psw'] = password_hash($data['psw'].$data['psw_salt'],PASSWORD_DEFAULT);
        $admin = new Admin();
        if($admin->save($data)){
            return info(true,'添加成功');
        }
        return info(false,'添加失败');
    }

    /**
     * 根据编号获取一行数据
     * @param int $id
     * @return Admin|array|Model|Json
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function getRow(int $id)
    {
        try {
            validate(\app\admin\validate\Admin::class)
                ->scene('id')
                ->check(['id'=>$id]);
        }
        catch (ValidateException $e) {
            return info(false, $e->getError());
        }
        $row = self::find($id);
        if($row) return $row;
        return [];
    }

    public static function edit(array $data)
    {
        try {
            validate(\app\admin\validate\Admin::class)
                ->scene('edit')
                ->check($data);
        }
        catch (ValidateException $e) {
            return info(false, $e->getError());
        }

        $row = self::find($data['id']);
        if(!$row) return info(false,'修改失败');
        $row->phone = $data['phone'];
        $row->email = $data['email'];
        $row->full_name = $data['full_name'];
        //如果用不输入密码，表示不修改密码
        if($data['psw']!="") {
            $psw_salt = Str::random(8);//随机生成密码盐
            $row->psw_salt = $data['psw_salt'];//保存密码盐
            $row->psw = password_hash($data['psw'] . $psw_salt, PASSWORD_DEFAULT);//加密密码
        }
        $row->status = $data['status'];
        if($row->save()){
           return info(true,'修改成功');
        }
        return info(false,'修改失败');
    }


    /**
     * 注册
     * @param $data
     */
    public static function reg($data)
    {
    }
}