<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\exception\ValidateException;
use think\Model;
use think\response\Json;

/**
 * @mixin \think\Model
 */
class Usergroup extends BaseMode
{
    /**
     * 删除
     * @param $id
     * @return Json
     */
    public static function del($id)
    {
        if(self::destroy($id)) return info(true,"删除成功！");
        return info(true,"删除失败！");

    }


    /**
     * 获取数据列表（带分页）
     * @param $parm
     * @return array
     * @throws DbException
     */
    public static function getList($parm){
        $where = self::initParams($parm);
        $list = self::order('user_id','asc')
            ->where($where)
            ->paginate($parm['limit'])
            ->each(function ($item, $key){
                $item['status'] = $item['status']==1?'启用':'禁用';

                $user = Admin::find($item['user_id']);
                $item['user'] =$user->user."(".$user->full_name.")";

                $group = Group::find($item['group_id']);
                $item['gname'] = $group->gname;

                return $item;
            });//根据编号倒序排序，每页显示10行
        return [
            'code'  => 0,  //不用修改
            'msg'   => '', //不用修改
            'count' => $list->total(), //获取数据库表中的数据总数
            'data'  => $list->items() //获取当前页面数据
        ];
    }

    /**
     * 保存添加的数据
     * @param array $data
     * @return Json
     */
    public static function saveAdd(array $data)
    {
        //验证数据的合法性
        try {
            validate(\app\admin\validate\Usergroup::class)
                ->scene('saveadd')
                ->check($data);
        }
        catch (ValidateException $e){
            return info(false,$e->getError());//返回验证的错误信息
        }
        //保存数据到数据库
        if(self::create($data)){
            return info(true,'添加成功！');
        }
        return info(false,'添加失败！');
    }

    /**
     * 根据编号获取一行数据
     * @param int $id
     * @return array|Json|Usergroup|Model
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function getRow($id)
    {
        try {
            validate(\app\admin\validate\Usergroup::class)
                ->scene('id')
                ->check(['id'=>$id]);
        }
        catch (ValidateException $e) {
            return info(false, $e->getError());
        }
        $row = self::find($id);
        if($row) return $row;
        return [];
    }

    public static function edit(array $data)
    {
        try {
            validate(\app\admin\validate\Usergroup::class)
                ->scene('edit')
                ->check($data);
        }
        catch (ValidateException $e) {
            return info(false, $e->getError());
        }

        $row = self::find($data['id']);
        if(!$row) return info(false,'修改失败！');
        $row->user_id = $data['user_id'];
        $row->group_id = $data['group_id'];
        $row->status    = $data['status'];
        if($row->save()){
            return info(true,'修改成功！');
        }
        return info(false,'修改失败！');
    }
}
