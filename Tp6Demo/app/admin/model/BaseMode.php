<?php


namespace app\admin\model;


use think\Model;
use think\model\concern\SoftDelete;

class BaseMode extends Model
{
    use SoftDelete;//启用软删除
    protected $deleteTime = 'delete_time';//指定删除标记的字段名
    protected $defaultSoftDelete = 0;//指定默认在显示的数据标记为0

    /**
     * 初始化搜索的参数
     * @param $params
     * @return array
     */
    public static function initParams($params){
        if(!isset($params['searchParams'])) return [];//如果搜索条件不存在，返回空数组
        if(!is_array($params['searchParams'])) return [];//如果索条件不是数组，返回空数组
        $temArr = [];
        foreach ($params['searchParams'] as $key=>$val){
            $temArr[] = [$key,'like',"%$val%"];
        }
        return $temArr;
    }

}