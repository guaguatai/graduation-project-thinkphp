<?php
declare (strict_types = 1);

namespace app\admin\model;

use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\exception\ValidateException;
use think\facade\Db;
use think\helper\Str;
use think\Model;
use think\response\Json;

/**
 * @mixin \think\Model
 */
class SpQualityinspection extends BaseMode
{

    /**
     * 删除
     * @param $id
     * @return Json
     */
    public static function del($id)
    {
        if(self::destroy($id)) return info(true,"质检信息删除成功啦");
        return info(true,"质检信息删除失败");

    }


    /**
     * 资源列表显示
     * @param $parm
     * @return array
     * @throws DbException
     */
    public static function getList($parm){
        $where = self::initParams($parm);
        $list = self::order('id','asc')
            ->where($where)
            ->paginate($parm['limit'])
            ->each(function ($item, $key){
                $item['status'] = $item['status']==1?'启用':'禁用';

                $pro = SpProduct::find($item['pro_name']);
                $item['pro_name'] = $pro->pro_name;

                $per = SpPersonnel::find($item['qua_quality']);
                $item['per_name'] = $per->per_name;

                return $item;
            });//根据编号倒序排序，每页显示10行
        return [
            'code'  => 0,  //不用修改
            'msg'   => '', //不用修改
            'count' => $list->total(), //获取数据库表中的数据总数
            'data'  => $list->items() //获取当前页面数据
        ];
    }

    /**
     * 保存添加的数据
     * @param array $data
     * @return Json
     */
    public static function saveAdd(array $data)
    {
        //验证数据的合法性
        try {
            validate(\app\admin\validate\SpQualityinspection::class)
                ->scene('saveadd')
                ->check($data);
        }
        catch (ValidateException $e){
            return info(false,$e->getError());//返回验证的错误信息
        }
        //保存数据到数据库
        $menu = new SpQualityinspection();
        if($menu->save($data)){
            return info(true,'质检信息添加成功啦');
        }
        return info(false,'质检信息添加失败');
    }

    /**
     * 根据编号获取一行数据
     * @param int $id
     * @return array|Json|Menu|Model
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function getRow($id)
    {
        try {
            validate(\app\admin\validate\SpQualityinspection::class)
                ->scene('id')
                ->check(['id'=>$id]);
        }
        catch (ValidateException $e) {
            return info(false, $e->getError());
        }
        $row = self::find($id);
        if($row) return $row;
        return [];
    }


    /**
     * 修改
     * @param array $data
     * @return Json
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public static function edit(array $data)
    {
        try {
            validate(\app\admin\validate\SpQualityinspection::class)
                ->scene('edit')
                ->check($data);
        }
        catch (ValidateException $e) {
            return info(false, $e->getError());
        }

        $row = self::find($data['id']);
        if(!$row) return info(false,'质检信息修改失败');
        $row->pro_name = $data['pro_name'];
        $row->qua_date = $data['qua_date'];
        $row->qua_quality = $data['qua_quality'];
        $row->qua_analysis = $data['qua_analysis'];
        $row->remark = $data['remark'];
        if($row->save()){
            return info(true,'质检信息修改成功啦');
        }
        return info(false,'质检信息修改失败');
    }
}
