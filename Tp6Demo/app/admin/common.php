<?php
// 这是系统自动生成的公共文件
use app\admin\model\Group;
use app\admin\model\Menu;
use app\admin\model\Usergroup;
use think\Collection;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;
use think\facade\Db;


/**
 * 获取提示信息
 * @param false $result 成功或者失败的标记（true,false）
 * @param string $msg 提示信息
 * @return \think\response\Json
 */
function info($result=false,$msg="操作失败"){
    $info =[
        'result' => $result,
        'msg' => $msg
    ];
    return json($info);
}

/**
 * 获取当前用户拥有的所有菜单
 * @return Menu[]|array|Collection
 * @throws DataNotFoundException
 * @throws DbException
 * @throws ModelNotFoundException
 */
function getMenus(){
    $usergroup = Usergroup::where('user_id', session('user_id'))->find();//获取分组编号
    $group = Group::find($usergroup['group_id']);//获取权限（菜单编号）

    $menuList = Menu::field('id,pid,title,icon,href,target,type')
        ->where('status', 1)
        ->order('sort', 'asc');


    if($group['menu_ids']!='*'){
        Db::query("call getPids('".$group['menu_ids']."',@menu_ids);");
        $menuids = Db::query('select @menu_ids;');

        return $menuList->where('id','in',$menuids[0]['@menu_ids'])->select();
    }
    return $menuList->select();
}

/**
 * 获取当前用户可以访问的链接
 * @param array $menuList
 * @return array
 */
function getAuthUrl($menuList=[]){
    $arrAuth = [];
    foreach ($menuList as $key => $vo){
        if($vo['href']){
            $arrAuth[] = $vo['href'];
        }
    }
    return $arrAuth;
}